package com.example.elihu.egame;

import java.util.*;

public class Battle {
	
	public Hero hero;
	public Enemy enemy;

	public Battle(Hero hero, Enemy enemy) 
	{
		this.hero = hero;
		this.enemy = enemy;
	}	
	
	@SuppressWarnings("resource")
	public Hero fight()//The Fight controller
	{
		Scanner key = new Scanner(System.in);
		boolean death = false;
		boolean enemyDeath = false;
		int enemyAttacked = 0;
		
		while(!death && !enemyDeath)//While no one dead, keep going
		{
			boolean ready = false;
			boolean enemyReady = false;
			
			
			while(!ready && !enemyReady)//While no one ready, increase time for both
			{
				if(hero.getSpeedCurrent() >= hero.getSpeedMax())
				{
					ready = true;
					enemyAttacked = 0;
				}
				else if(enemy.getSpeedCurrent() >= enemy.getSpeedMax())
				{
					enemyReady = true;
					enemyAttacked++;
				}
				else {
					hero.time();
					enemy.time();
				}
			}
			
			if(enemyAttacked<2)
			{
				String levelHero = "Level " + hero.getLevel() + "\n";
				String hp = hero.getName() + "\nHP: (" + hero.getCurrentHp() + "|" + hero.getMaxHp() + ")\n";
				System.out.println(levelHero + hp);
				
				String levelEnemy = "Level " + enemy.getLevel() + "\n";
				String hpEnemy = enemy.getName() + "\nHP: (" + enemy.getCurrentHp() + "|" + enemy.getMaxHp() + ")\n";
				System.out.println(levelEnemy + hpEnemy);
			}							
			
			if(enemyReady)//If enemy ready, his turn
			{
				hero.attackReceived(enemy);
				enemy.restartTime();
				enemyReady = false;
				if(hero.getCurrentHp() <= 0)
				{
					MyGame.textToScreen("YOU DIED !!!!     ");
					death = true;
					enemy.refresh();
					return hero;					
				}	
			}
			while(ready)//While user ready, battle menu
			{
				System.out.println("Your turn! \n");
				System.out.println("Attack (A)");
				System.out.println("Items (I)");
				System.out.println("Scan Enemy (S)");
				String selection = key.nextLine();
				System.out.println("--------------------------------------------------------");
				if(selection.equalsIgnoreCase("a"))
				{
					hero.restartTime();
					ready = false;
					enemy.attackReceived(hero);
				}
				else if(selection.equalsIgnoreCase("i"))
				{
					hero.restartTime();
					ready = hero.itemsBattle();
				}
				else if(selection.equalsIgnoreCase("s"))
				{
					hero.restartTime();
					ready = false;
					MyGame.textToScreen("Scanning ........");
					System.out.println("\n" + enemy);
					MyGame.textToScreen("Scan Complete!       ");
				}
				else if(selection.equalsIgnoreCase("h"))
				{
					hero.restartTime();
					ready = false;
					int healed = hero.increaseCurrentHp(0.2);
					if(healed > 0)
					{
						System.out.println("===================================");
						System.out.println("You have healed yourself (+" + healed + " HP)");
						System.out.println("===================================");
					}
					else {
						System.out.println("===================================");
						System.out.println("You are already at full HP");
						System.out.println("===================================");
					}
				}else {
					System.out.println("===================================");
					System.out.println("You did nothing! Well done!");
					System.out.println("===================================");
				}
				if(enemy.getCurrentHp() <= 0)
				{
						enemyDeath();
						enemyDeath = true;
				}				
				System.out.println("--------------------------------------------------------");
			}				
		}
		return hero;
	}
	
	public Hero enemyDeath()//When enemy dies, calculate drops, experience etc..
	{
		System.out.println("It's dead!! stop hitting it!");
		System.out.println("You have earned " + enemy.getExperience() + " Experience");
		
		for(int index = 0; index<enemy.getDrops().size(); index++)//Get drops collected based on chance
		{
			Item drop = collectDrops(index);//Collects drop if chance is met
			if(drop != null)//If drop found, add to hero list
			{
				if(drop instanceof ItemsHero)
				{
					ItemsHero itemFound = (ItemsHero)hero.findItem(drop);
					if(itemFound == null)
					{
						hero.addItem((ItemsHero)drop);
					}
					else {
						itemFound.increaseQuantity(1);
					}
					System.out.println(enemy.getName() + " dropped " + drop.getName());
				}
				if(drop instanceof Weapon)
				{
					Weapon itemFound = (Weapon)hero.findWeapon(drop);
					if(itemFound == null)
					{
						hero.addWeapon((Weapon)drop);
						System.out.println(enemy.getName() + " dropped " + drop.getName());
					}	
				}
				if(drop instanceof Armour)
				{
					Armour itemFound = (Armour)hero.findArmour(drop);
					if(itemFound == null)
					{
						hero.addArmour((Armour)drop);
						System.out.println(enemy.getName() + " dropped " + drop.getName());
					}	
				}
			}						
		}				
		hero.increaseExperience(enemy.getExperience());
		enemy.refresh();
		return hero;
	}
	
	public Item collectDrops(int index)//Returns drop if chance is met
	{
		List<Item> tempDrops = enemy.getDrops();
		try {
			Item drop = tempDrops.get(index);
			double chance = Math.random();
			if(drop.getChance()>chance)
			{
				return drop;
			}
			return null;
		}
		catch(Exception e)
		{
		}
		return null;
	}
	
}
