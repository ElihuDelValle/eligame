package com.example.elihu.egame;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.List;

public class HeroScreen extends AppCompatActivity implements View.OnClickListener {

    Button exitButton;
    Button increaseStr;
    Button increaseAgi;
    Button increaseVit;
    Button weaponsBtn;
    Button armoursBtn;
    Button itemsBtn;
    MyGame activeGame;
    Hero hero;
    TextView heroName;
    TextView heroWeapon;
    TextView heroArmour;
    TextView heroLevel;
    TextView heroHp;
    TextView heroStr;
    TextView heroAgi;
    TextView heroVit;
    TextView heroAttack;
    TextView heroDefence;
    TextView heroDodge;
    TextView heroSpeed;
    TextView heroAccuracy;
    TextView heroToNextLvl;
    TextView heroStatPoints;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hero_screen);

        activeGame = (MyGame) getIntent().getSerializableExtra("serialize_data");
        hero = activeGame.getHero();

        loadViews();
        refreshScreen();

        exitButton.setOnClickListener(this);
        increaseStr.setOnClickListener(this);
        increaseAgi.setOnClickListener(this);
        increaseVit.setOnClickListener(this);
        weaponsBtn.setOnClickListener(this);
        armoursBtn.setOnClickListener(this);
        itemsBtn.setOnClickListener(this);
    }

    public void loadViews()
    {
        exitButton = (Button) findViewById(R.id.heroScreenExitBtn);
        increaseAgi = (Button) findViewById(R.id.increaseAgiButton);
        increaseStr = (Button) findViewById(R.id.increaseStrButton);
        increaseVit = (Button) findViewById(R.id.increaseVitButton);
        weaponsBtn = (Button) findViewById(R.id.weaponsButton);
        armoursBtn = (Button) findViewById(R.id.armoursButton);
        itemsBtn = (Button) findViewById(R.id.itemsButton);

        heroAttack = (TextView) findViewById(R.id.attackStat);
        heroDefence = (TextView) findViewById(R.id.defenceStat);
        heroDodge = (TextView) findViewById(R.id.dodgeStat);
        heroSpeed = (TextView) findViewById(R.id.speedStat);
        heroAccuracy = (TextView) findViewById(R.id.accuracyStat);
        heroToNextLvl = (TextView) findViewById(R.id.nextLevelStat);
        heroStatPoints = (TextView) findViewById(R.id.statPoints);

        heroName = (TextView) findViewById(R.id.heroName);
        heroWeapon = (TextView) findViewById(R.id.weaponName);
        heroArmour = (TextView) findViewById(R.id.armourName);

        heroLevel = (TextView) findViewById(R.id.levelStat);
        heroHp = (TextView) findViewById(R.id.hpStat);
        heroStr = (TextView) findViewById(R.id.strStat);
        heroAgi = (TextView) findViewById(R.id.agiStat);
        heroVit = (TextView) findViewById(R.id.vitStat);
    }

    public void refreshScreen()
    {
        if(hero.getStatPoints()== 0)
        {
            increaseAgi.setVisibility(View.GONE);
            increaseStr.setVisibility(View.GONE);
            increaseVit.setVisibility(View.GONE);
        }
        else{
            increaseVit.setVisibility(View.VISIBLE);
            increaseStr.setVisibility(View.VISIBLE);
            increaseAgi.setVisibility(View.VISIBLE);
        }
        if(hero.getArmoursOwned().size() == 0)
        {
            armoursBtn.setVisibility(View.GONE);
        }
        else{
            armoursBtn.setVisibility(View.VISIBLE);
        }
        if(hero.getWeaponsOwned().size() == 0)
        {
            weaponsBtn.setVisibility(View.GONE);
        }
        else{
            weaponsBtn.setVisibility(View.VISIBLE);
        }
        if(hero.getItems().size() == 0)
        {
            itemsBtn.setVisibility(View.GONE);
        }
        else{
            itemsBtn.setVisibility(View.VISIBLE);
        }

        String heroNameString = activeGame.getHero().getName();
        String levelString = Integer.toString(hero.getLevel());
        String currentHpString = Integer.toString(hero.getCurrentHp());
        String maxHpString = Integer.toString(hero.getMaxHp());
        String hpString = currentHpString + "/" + maxHpString;
        String strString = Integer.toString(hero.getStrength());
        String vitString = Integer.toString(hero.getVitality());
        String agiString = Integer.toString(hero.getAgility());

        String attackString = Integer.toString(hero.getAttack());
        String defenceString = Integer.toString(hero.getDefence());
        String dodgeString = Integer.toString(hero.getDodge()) + "%";
        String speedString = Integer.toString(hero.getSpeed());
        String accuracyString = Integer.toString(hero.getAccuracy()) + "%";
        String nextLevelString = Integer.toString(hero.getExperienceRemaining());
        String statPointsString = Integer.toString(hero.getStatPoints());

        String weaponString;
        try {
            weaponString = hero.getWeaponEquipped().getName();
        }catch (Exception e){weaponString = "None";}
        String armourString;
        try {
            armourString = hero.getArmourEquipped().getName();
        }catch (Exception e){armourString = "None";}


        heroName.setText(heroNameString);
        heroWeapon.setText(weaponString);
        heroArmour.setText(armourString);
        heroLevel.setText(levelString);
        heroHp.setText(hpString);
        heroStr.setText(strString);
        heroAgi.setText(agiString);
        heroVit.setText(vitString);

        heroAttack.setText(attackString);
        heroDefence.setText(defenceString);
        heroDodge.setText(dodgeString);
        heroSpeed.setText(speedString);
        heroAccuracy.setText(accuracyString);
        heroToNextLvl.setText(nextLevelString);
        heroStatPoints.setText(statPointsString);
    }

    @Override
    public void onClick(View view) {
        /*if(view.getId() == R.id.deleteButton)
        {
            try {
                FileOutputStream fos;
                fos = new FileOutputStream(new File(this.getFilesDir(), "m1"));
                ObjectOutputStream oos;
                oos = new ObjectOutputStream(fos);
                oos.writeObject(null);
                oos.close();
            } catch (Exception e1) {
                e1.printStackTrace();
            }
            Context context = getApplicationContext();
            Toast myToast = Toast.makeText(context,"Info Deleted!",Toast.LENGTH_LONG);
            myToast.show();
            Intent intent = new Intent(this,MainActivity.class);
            startActivity(intent);
        }
        else if(view.getId() == R.id.testButton)
        {
            hero.increaseExperience(hero.getExperienceRemaining());
            hero.levelUp();
            hero.calculateStats();
            refreshStats();
            Context context = getApplicationContext();
            Toast myToast = Toast.makeText(context,"Leveled Up!!",Toast.LENGTH_LONG);
            myToast.show();
        }*/
        if(view.getId() == R.id.heroScreenExitBtn)
        {
            Intent intent = new Intent(this,MainScreen.class);
            intent.putExtra("serialize_data",activeGame);
            startActivity(intent);
        }
        else if(view.getId()==R.id.increaseStrButton)
        {
            hero.increaseStr(1);
            hero.increaseStatPoints(-1);
            hero.calculateStats();
            Armour testArmour = new Armour("Bless of the Gods");
            hero.addArmour(testArmour);
            Armour testArmour2 = new Armour("Jedi Robe");
            hero.addArmour(testArmour2);
            Armour testArmour3 = new Armour("Mythril Plate");
            hero.addArmour(testArmour3);
            refreshScreen();

        }
        else if(view.getId()==R.id.increaseAgiButton)
        {
            hero.increaseAgi(1);
            hero.increaseStatPoints(-1);
            hero.calculateStats();
            Weapon testWeapon = new Weapon("Elinathan");
            hero.addWeapon(testWeapon);
            Weapon testWeapon2 = new Weapon("Lightsaber");
            hero.addWeapon(testWeapon2);
            Weapon testWeapon3 = new Weapon("Excalibur");
            hero.addWeapon(testWeapon3);
            Weapon testWeapon4 = new Weapon("Katana");
            hero.addWeapon(testWeapon4);
            Weapon testWeapon5 = new Weapon("Ice");
            hero.addWeapon(testWeapon5);
            Weapon testWeapon6 = new Weapon("Needle");
            hero.addWeapon(testWeapon6);
            Weapon testWeapon7 = new Weapon("Steel Sword");
            hero.addWeapon(testWeapon7);
            Weapon testWeapon8 = new Weapon("Javelin");
            hero.addWeapon(testWeapon8);
            refreshScreen();
        }
        else if(view.getId()==R.id.increaseVitButton)
        {
            hero.increaseVit(1);
            hero.increaseStatPoints(-1);
            hero.calculateStats();
            Item testApple = new Item("Apple");
            hero.addItem(testApple);
            Item testOrange = new Item("Orange");
            hero.addItem(testOrange);
            List<ItemsHero> test = hero.getItems();
            ItemsHero test2 = test.get(0);
            test2.increaseQuantity(2);
            refreshScreen();
        }
        else if(view.getId()==R.id.weaponsButton)
        {
            Intent intent = new Intent(this,WeaponsListScreen.class);
            intent.putExtra("serialize_data",activeGame);
            startActivity(intent);
        }
        else if(view.getId()==R.id.armoursButton)
        {
            Intent intent = new Intent(this,ArmoursListScreen.class);
            intent.putExtra("serialize_data",activeGame);
            startActivity(intent);
        }
        else if(view.getId()==R.id.itemsButton)
        {
            Intent intent = new Intent(this,ItemsListScreen.class);
            intent.putExtra("serialize_data",activeGame);
            startActivity(intent);
        }
    }
}
