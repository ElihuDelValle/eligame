package com.example.elihu.egame;

public class Item implements java.io.Serializable{

	private static final long serialVersionUID = 1L;
	private String name;
	protected String description;
	protected double chance;

	public Item(String name)
	{
		this.name = name;
	}
	// Modifiers -------------------
	public void setChance(double newChance)
	{
		chance = newChance;
	}
	// Getters ---------------------
	
	public String getName()
	{
		return name;
	}

	public double getChance()
	{
		return chance;
	}
	// Representation
	public String toString()
	{
		return name + " : " +  chance;
	}

}
