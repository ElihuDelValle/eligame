package com.example.elihu.egame;

public class ItemsHero extends Item{

	private static final long serialVersionUID = 1L;

	private Item item;
	private int quantity;
	private double points;
	private String type;
	
	public ItemsHero(Item item){
		super(item.getName());
		this.item = item;
		quantity = 1;
		
		if(item.getName().equalsIgnoreCase("bad apple"))
		{
			type = "hp";
			description = "An apple....looks overripped";
			points = 20;
		}
		else if(item.getName().equalsIgnoreCase("bad orange"))
		{
			type = "hp";
			points = 40;
			description = "An orange....looks overripped";
		}
		else if(item.getName().equalsIgnoreCase("apple"))
		{
			type = "hp";
			points = 120;
			description = "A delicious apple";
		}
		else if(item.getName().equalsIgnoreCase("orange"))
		{
			type = "hp";
			points = 200;
			description = "A delicious orange";
		}
		else if(item.getName().equalsIgnoreCase("banana"))
		{
			type = "hp";
			points = .05;
			description = "A banana, lots of energy!";
		}
		else if(item.getName().equalsIgnoreCase("raspberry"))
		{
			type = "hp";
			points = .1;
			description = "This fruit is something special...";
		}
		else if(item.getName().equalsIgnoreCase("lamington"))
		{
			type = "hp";
			points = .3;
			description = "Delicious cake with chocolate and coconut";
		}
		else if(item.getName().equalsIgnoreCase("pavlova"))
		{
			type = "hp";
			points = .5;
			description = "Pretty much sugar with fruit..so good!";
		}
		else if(item.getName().equalsIgnoreCase("chocolate block"))
		{
			type = "hp";
			points = .65;
			description = "Big block of chocolate...nothing else to say..";
		}
		else if(item.getName().equalsIgnoreCase("vanilla slice"))
		{
			type = "hp";
			points = .95;
			description = "Tastiest ever!";
		}
		else if(item.getName().equalsIgnoreCase("milk"))
		{
			type = "hp";
			points = 2000;
			description = "Fresh milk, good for the muscles!";
		}
	}
	
	// Modifiers --------------------------------
	public void increaseQuantity(int amount)
	{
		quantity += amount;
	}
	
	// Getters ---------------------------
	public Item getItem()
	{
		return item;
	}
	
	public int getQuantity()
	{
		return quantity;
	}
	
	public String getType()
	{
		return type;
	}
	
	public double getPoints()
	{
		return points;
	}
	
	// Representation ------------------------------
	public String toString()
	{
		String nameTemp = getName();
		String healTemp = "";
		if(points<1.1)
		{
			healTemp = "Heals " + (points*100) + "% of your max HP";
		}
		else {
			healTemp = "Heals " + points + " HP";
		}
		return nameTemp + " (" + quantity + ")\n" + description + "\n" + healTemp;
	}
	
}
