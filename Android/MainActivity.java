package com.example.elihu.egame;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button startBtn;
    Button continueBtn;
    MyGame activeGame;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        continueBtn = (Button) findViewById(R.id.continueBtn);
        startBtn = (Button) findViewById(R.id.startBtn);

        continueBtn.setVisibility(View.GONE);

        startBtn.setOnClickListener(this);
        continueBtn.setOnClickListener(this);

        activeGame = loadGame();
        continueButton(activeGame);
    }

    @Override
    public void onClick(View view) {
        if(view.getId()==R.id.startBtn)
        {
            Intent intent = new Intent(this, NameHero.class);
            startActivity(intent);
        }
        else if(view.getId()==R.id.continueBtn)
        {
            Intent intent = new Intent(this, MainScreen.class);
            intent.putExtra("serialize_data",activeGame);
            startActivity(intent);
        }
    }

    public MyGame loadGame() //Loads game information
    {
        boolean fileExist1 = false;
        MyGame slot1 = null;

        ObjectInputStream o1 = null;
        FileInputStream m1 = null;

        //Checking if the 3 memory files got any object saved and reads them
        while(!fileExist1)
        {
            //--- Checking memory slot 1-------------------------------------
            try {
                m1 = new FileInputStream(new File(this.getFilesDir(),"m1"));
                fileExist1 = true;
            }
            catch(Exception e) {
                try {
                    FileOutputStream fos;
                    fos = new FileOutputStream(new File(this.getFilesDir(), "m1"));
                    ObjectOutputStream oos;
                    oos = new ObjectOutputStream(fos);
                    oos.writeObject(null);
                    oos.close();
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        }
        //--- Reading the object and assigning it to memory slot----------
        try {
            o1 = new ObjectInputStream(m1);
            slot1 = (MyGame) o1.readObject();
            o1.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return slot1;
    }

    public void continueButton(MyGame game) // Shows menu and allows user to select memory slot to play on
    {
        if(game != null)
        {
            continueBtn.setVisibility(View.VISIBLE);
            continueBtn.setText("Continue");
        }
    }

}
