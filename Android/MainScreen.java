package com.example.elihu.egame;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class MainScreen extends AppCompatActivity implements View.OnClickListener {

    MyGame activeGame;
    Button exploreBtn;
    Button heroBtn;
    Button saveBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_screen);
        activeGame = (MyGame) getIntent().getSerializableExtra("serialize_data");
        exploreBtn = (Button) findViewById(R.id.exploreButton);
        heroBtn = (Button) findViewById(R.id.heroButton);
        saveBtn = (Button) findViewById(R.id.saveButton);

        heroBtn.setOnClickListener(this);
        exploreBtn.setOnClickListener(this);
        saveBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.heroButton)
        {
            Intent intent = new Intent(this, HeroScreen.class);
            intent.putExtra("serialize_data",activeGame);
            startActivity(intent);
        }
        else if(view.getId() == R.id.saveButton)
        {
            save(activeGame);
            Context context = getApplicationContext();
            Toast myToast = Toast.makeText(context,"Succesfully Saved",Toast.LENGTH_LONG);
            myToast.show();
        }
        else if (view.getId() == R.id.exploreButton)
        {
            Intent intent = new Intent(this,ExploreScreen.class);
            intent.putExtra("serialize_data",activeGame);
            startActivity(intent);
        }
    }

    public void save(MyGame game){
        try {
            String slot = "m1" ;
            FileOutputStream fos = new FileOutputStream(new File(this.getFilesDir(),slot));
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(game);
        }
        catch(IOException e) {
            e.getStackTrace();
        }
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}
