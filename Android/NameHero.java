package com.example.elihu.egame;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class NameHero extends AppCompatActivity implements View.OnClickListener, Serializable {

    EditText heroName;
    Button readyButton;
    MyGame activeGame;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_name_hero);


        readyButton = (Button) findViewById(R.id.readyButton);
        heroName = (EditText) findViewById(R.id.heroNameInput);
        readyButton.setOnClickListener(this);

    }

    public void onClick(View view)
    {
        String heroNameInput = heroName.getText().toString();
        try {
            activeGame = new MyGame(heroNameInput);
        } catch (Exception e) {
            e.printStackTrace();
        }
        save(activeGame);
        Intent intent = new Intent(this, MainScreen.class);
        intent.putExtra("serialize_data",activeGame);
        startActivity(intent);
    }

    public void save(MyGame game){
        try {
            String slot = "m1" ;
            FileOutputStream fos = new FileOutputStream(new File(this.getFilesDir(),slot));
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(game);
        }
        catch(IOException e) {
            e.getStackTrace();
        }
    }


    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}
