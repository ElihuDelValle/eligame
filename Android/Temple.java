package com.example.elihu.egame;

import java.util.ArrayList;
import java.util.List;

public class Temple implements java.io.Serializable{

	private static final long serialVersionUID = 1L;
	
	private int floors;
	private List<Enemy> enemies;
	private Enemy boss;
	private String status;
	private int currentFloor;
	private String name;
	private boolean discovered;
	
	public Temple(String name)
	{
		this.name = name;
		discovered = false;
		status = "uncleared";
		currentFloor = 1;
		enemies = new ArrayList<Enemy>();
		if(name.equalsIgnoreCase("T1"))
		{
			floors = 5; 
		}
		if(name.equalsIgnoreCase("T2"))
		{
			floors = 7; 
		}
		if(name.equalsIgnoreCase("T3"))
		{
			floors = 10; 
		}
	}
	
	public void setBoss(Enemy boss)
	{
		this.boss = boss;
	}
	
	public void addEnemies(Enemy enemy)
	{
		enemies.add(enemy);
	}
	
	public int getFloors()
	{
		return floors;
	}
	
	public String getStatus()
	{
		return status;
	}
	
	public Enemy getEnemy(int index)
	{
		return enemies.get(index);
	}
	
	public Enemy getBoss()
	{
		return boss;
	}
	
	public List<Enemy> getEnemies()
	{
		return enemies;
	}
	
	public void cleared()
	{
		status = "cleared";
	}
	
	public void increaseFloor()
	{
		currentFloor++;
	}
	
	public void resetFloor()
	{
		currentFloor = 1;
	}
	
	public boolean getDiscovered()
	{
		return discovered;
	}

}
