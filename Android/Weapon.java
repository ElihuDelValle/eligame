package com.example.elihu.egame;

public class Weapon extends Item implements java.io.Serializable{

	private static final long serialVersionUID = 1L;
	private int strBonus;
	private int vitBonus;
	private int agiBonus;
	private int attackBonus;

	public Weapon(String name){
		super(name);
		strBonus = 0;
		agiBonus = 0;
		vitBonus = 0;
		attackBonus = 1;
		//Swords--------------------------------
		if(name.equalsIgnoreCase("Wooden Stick"))
		{
			attackBonus = 40;
		}
		else if(name.equalsIgnoreCase("Wooden Sword"))
		{
			attackBonus = 62;
		}
		else if(name.equalsIgnoreCase("Iron Sword"))
		{
			attackBonus = 200;
			strBonus = 2;
			vitBonus = 0;
			agiBonus = 1;
		}
		else if(name.equalsIgnoreCase("Javelin"))
		{
			attackBonus = 85;
			strBonus = 3;
			vitBonus = 0;
			agiBonus = 2;
		}
		else if(name.equalsIgnoreCase("Steel Sword"))
		{
			attackBonus = 150;
			strBonus = 5;
			vitBonus = 1;
			agiBonus = 3;
		}
		else if(name.equalsIgnoreCase("Needle"))
		{
			attackBonus = 200;
			strBonus = 8;
			vitBonus = 1;
			agiBonus = 10;
		}
		else if(name.equalsIgnoreCase("Ice"))
		{
			attackBonus = 1200;
			strBonus = 30;
			vitBonus = 1;
			agiBonus = 1;
		}
		else if(name.equalsIgnoreCase("Oathkeeper"))
		{
			attackBonus = 1000;
			strBonus = 33;
			vitBonus = 17;
			agiBonus = 22;
		}
		else if(name.equalsIgnoreCase("Katana"))
		{
			attackBonus = 1500;
			strBonus = 40;
			vitBonus = 10;
			agiBonus = 20;
		}
		else if(name.equalsIgnoreCase("Lightsaber"))
		{
			attackBonus = 1200;
			strBonus = 35;
			vitBonus = 15;
			agiBonus = 25;
		}
		else if(name.equalsIgnoreCase("Excalibur"))
		{
			attackBonus = 3333;
			strBonus = 50;
			vitBonus = 25;
			agiBonus = 50;
		}
		else if(name.equalsIgnoreCase("Elinathan"))
		{
			attackBonus = 5000;
			strBonus = 100;
			vitBonus = 50;
			agiBonus = 100;
					
		}
	}
	
	//Getters  --------------------------------
	public String getName()
	{
		return super.getName();
	}
	
	public int getStrengthBonus()
	{
		return strBonus;
	}
	
	public int getAgilityBonus()
	{
		return agiBonus;
	}
	
	public int getVitalityBonus()
	{
		return vitBonus;
	}
	
	public int getAttackBonus()
	{
		return attackBonus;
	}
	
	// Representation  --------------------------
	public String toString()
	{
		String nameTemp = getName();
		String strBonusTemp = "Str: +" + strBonus + "\n";
		String agiBonusTemp = "Agi: +" + agiBonus + "\n";
		String vitBonusTemp = "Vit: +" + vitBonus + "\n";
		String attackBonusTemp = "Attack: +" + attackBonus + "\n";
		if(agiBonus == 0)
		{
			agiBonusTemp = "";
		}
		if(strBonus == 0)
		{
			strBonusTemp = "";
		}
		if(vitBonus == 0)
		{
			vitBonusTemp = "";
		}
		
		String finalString = nameTemp + "\n"
				+ attackBonusTemp + strBonusTemp + agiBonusTemp + vitBonusTemp;
		
		return finalString;

	}
	
}
