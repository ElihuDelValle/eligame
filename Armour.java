package eligame;

public class Armour extends Item implements java.io.Serializable{

	private static final long serialVersionUID = 1L;
	private int strBonus;
	private int vitBonus;
	private int agiBonus;
	private int defenceBonus;
	
	public Armour(String name) {
		super(name);
		strBonus = 0;
		vitBonus = 0;
		agiBonus = 0;
		defenceBonus = 0;
		if(name.equalsIgnoreCase("T-Shirt (I <3 NY)"))
		{
			defenceBonus = 60;
		}
		else if(name.equalsIgnoreCase("Rags"))
		{
			defenceBonus = 30;
		}
		else if(name.equalsIgnoreCase("Leather Vest"))
		{
			defenceBonus = 5;
		}
		else if(name.equalsIgnoreCase("Iron Armour"))
		{
			defenceBonus = 20;
			strBonus = 0;
			vitBonus = 2;
			agiBonus = -5;
		}
		else if(name.equalsIgnoreCase("Steel Armour"))
		{
			defenceBonus = 55;
			strBonus = 0;
			vitBonus = 5;
			agiBonus = -1;
		}
		else if(name.equalsIgnoreCase("Mithril Armour"))
		{
			defenceBonus = 100;
			strBonus = 3;
			vitBonus = 10;
			agiBonus = 0;
		}
		else if(name.equalsIgnoreCase("Nano Tech Armour"))
		{
			defenceBonus = 300;
			strBonus = 10;
			vitBonus = 30;
			agiBonus = 5;
		}
		else if(name.equalsIgnoreCase("Bless of the Gods"))
		{
			defenceBonus = 1000;
			strBonus = 30;
			vitBonus = 65;
			agiBonus = 20;
		}
		
		else if(name.equalsIgnoreCase("E-Aura"))
		{
			defenceBonus = 3333;
			strBonus = 50;
			vitBonus = 100;
			agiBonus = 50;
		}
	}
	
	// Getters -----------------------------
	public String getName()
	{
		return super.getName();
	}
	
	public int getStrengthBonus()
	{
		return strBonus;
	}
	
	public int getAgilityBonus()
	{
		return agiBonus;
	}
	
	public int getVitalityBonus()
	{
		return vitBonus;
	}
	
	public int getDefenceBonus()
	{
		return defenceBonus;
	}
	
	// Representation  ----------------------------
	public String toString()
	{
		String nameTemp = getName();
		String strBonusTemp = "Str: +" + strBonus + "\n";
		String agiBonusTemp = "Agi: +" + agiBonus + "\n";
		String vitBonusTemp = "Vit: +" + vitBonus + "\n";
		String defenceBonusTemp = "Defence: +" + defenceBonus + "\n";
		if(agiBonus == 0)
		{
			agiBonusTemp = "";
		}
		if(strBonus == 0)
		{
			strBonusTemp = "";
		}
		if(vitBonus == 0)
		{
			vitBonusTemp = "";
		}
		
		String finalString = nameTemp + "\n"
				+ defenceBonusTemp + strBonusTemp + agiBonusTemp + vitBonusTemp;
		
		return finalString;
	}
	
}