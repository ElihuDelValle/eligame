package com.example.elihu.egame;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class ArmoursListScreen extends AppCompatActivity implements View.OnClickListener {

    MyGame activeGame;
    List<Armour> armours;
    ArrayList<String> armoursNames;
    Button exitBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_armours_list_screen);
        exitBtn = (Button) findViewById(R.id.exitArmoursScreenButton);
        exitBtn.setOnClickListener(this);
        activeGame = (MyGame) getIntent().getSerializableExtra("serialize_data");
        armours = activeGame.getHero().getArmoursOwned();
        armoursNames = new ArrayList<String>();
        for(Armour a:armours)
        {
            armoursNames.add(a.getName());
        }
        ListView listView = (ListView)findViewById(R.id.armoursList);
        final ArrayAdapter<Armour> adapter = new ArrayAdapter<Armour>(listView.getContext(),android.R.layout.simple_list_item_1,armours);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Armour armourSelected = adapter.getItem(i);
                activeGame.getHero().equipArmour(armourSelected);
                Context context = getApplicationContext();
                String message = "You have equipped " + armourSelected.getName();
                Toast toastie = Toast.makeText(context,message,Toast.LENGTH_SHORT);
                toastie.show();
            }
        });
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.exitArmoursScreenButton)
        {
            Intent intent = new Intent(this,HeroScreen.class);
            intent.putExtra("serialize_data",activeGame);
            startActivity(intent);
        }
    }
}
