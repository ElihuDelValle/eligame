package com.example.elihu.egame;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class ExploreScreen extends AppCompatActivity {

    MyGame activeGame;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_explore_screen);
        activeGame = (MyGame) getIntent().getSerializableExtra("serialize_data");

        Bundle bundle = new Bundle();
        bundle.putSerializable("test",activeGame);
        //getFragmentManager().findFragmentById(R.id.fragment).setArguments(bundle);

        ExploreWindow exploreWindow = (ExploreWindow) getFragmentManager().findFragmentById(R.id.fragment);

        exploreWindow.initData(bundle);
        //getFragmentManager().beginTransaction().commit();
        ControllerWindow controllerWindow = (ControllerWindow) getFragmentManager().findFragmentById(R.id.fragment2);
    }
}
