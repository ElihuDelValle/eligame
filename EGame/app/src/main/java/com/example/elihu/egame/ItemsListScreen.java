package com.example.elihu.egame;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class ItemsListScreen extends AppCompatActivity implements View.OnClickListener {

    MyGame activeGame;
    Hero hero;
    List<ItemsHero> items;
    ArrayList<String> itemsName;
    ListView listV;
    Button exitBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_items_list_screen);
        exitBtn = (Button) findViewById(R.id.exitItemsScreenButton);
        exitBtn.setOnClickListener(this);
        activeGame = (MyGame) getIntent().getSerializableExtra("serialize_data");
        hero = activeGame.getHero();
        items = hero.getItems();
        itemsName = new ArrayList<String>();
        for(ItemsHero i:items)
        {
            itemsName.add(i.getName());
        }
        ListView listView = (ListView)findViewById(R.id.itemsList);
        final ArrayAdapter<ItemsHero> adapter = new ArrayAdapter<ItemsHero>(listView.getContext(),android.R.layout.simple_list_item_1,items);
        listView.setAdapter(adapter);
        listV = listView;
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                ItemsHero itemSelected = adapter.getItem(i);
                itemSelected.increaseQuantity(-1);
                if(itemSelected.getType().equalsIgnoreCase("hp"))//If type HP, heals HP
                {
                    double amountHealed;
                    double heal = itemSelected.getPoints();
                    if(heal>(1.1))
                    {
                        int healInt = (int)heal;
                        amountHealed = hero.increaseCurrentHp(healInt);
                    }
                    else {
                        amountHealed = hero.increaseCurrentHp(heal);
                    }
                    Context context = getApplicationContext();
                    String message = "You have healed " + ((int)amountHealed) + " HP";
                    Toast toastie = Toast.makeText(context,message,Toast.LENGTH_SHORT);
                    toastie.show();
                    if(itemSelected.getQuantity() == 0)//If reaches 0. delete items from list
                    {
                        int indexTemp = items.indexOf(itemSelected);
                        items.remove(indexTemp);
                        if(items.size() == 0)
                        {
                            Intent intent = new Intent(context,HeroScreen.class);
                            intent.putExtra("serialize_data",activeGame);
                            startActivity(intent);
                        }
                    }
                }
                load(listV,adapter);
            }
        });
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.exitItemsScreenButton)
        {
            Intent intent = new Intent(this,HeroScreen.class);
            intent.putExtra("serialize_data",activeGame);
            startActivity(intent);
        }
    }

    public void load(ListView l, ArrayAdapter<ItemsHero> a){
        l.setAdapter(a);
    }
}
