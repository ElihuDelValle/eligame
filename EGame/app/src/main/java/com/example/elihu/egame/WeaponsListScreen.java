package com.example.elihu.egame;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class WeaponsListScreen extends AppCompatActivity implements View.OnClickListener {

    MyGame activeGame;
    List<Weapon> weapons;
    ArrayList<String> weaponsNames;
    Button exitBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weapons_list_screen);
        exitBtn = (Button) findViewById(R.id.exitWeaponsScreenButton);
        exitBtn.setOnClickListener(this);
        activeGame = (MyGame) getIntent().getSerializableExtra("serialize_data");
        weapons = activeGame.getHero().getWeaponsOwned();
        weaponsNames = new ArrayList<String>();
        for(Weapon w:weapons)
        {
            weaponsNames.add(w.getName());
        }
        ListView listView = (ListView)findViewById(R.id.weaponsList);
        final ArrayAdapter<Weapon> adapter = new ArrayAdapter<Weapon>(listView.getContext(),android.R.layout.simple_list_item_1,weapons);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Weapon weaponSelected = adapter.getItem(i);
                activeGame.getHero().equipWeapon(weaponSelected);
                Context context = getApplicationContext();
                String message = "You have equipped " + weaponSelected.getName();
                Toast toastie = Toast.makeText(context,message,Toast.LENGTH_SHORT);
                toastie.show();
            }
        });
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.exitWeaponsScreenButton)
        {
            Intent intent = new Intent(this,HeroScreen.class);
            intent.putExtra("serialize_data",activeGame);
            startActivity(intent);
        }
    }
}
