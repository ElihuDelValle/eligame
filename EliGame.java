package eligame;

import java.io.*;
import java.util.*;

public class EliGame{
	
	public static void main(String[] args) throws IOException 
	{
		MyGame[] games;
		MyGame activeGame = null;
		while(activeGame == null)
		{
			games = loadGames();
			activeGame = mainMenu(games);
			activeGame = verifyGame(activeGame);
		}
		activeGame.story();
		System.out.println("Looks good!");
	}
		
	public static MyGame[] loadGames()throws IOException //Loads game information
	{
		boolean fileExist1 = false;
		boolean fileExist2 = false;
		boolean fileExist3 = false;
		
		MyGame[] slots = new MyGame[3];
			
		MyGame slot1 = null;
		MyGame slot2 = null;
		MyGame slot3 = null;
		
		ObjectInputStream o1 = null;
		ObjectInputStream o2 = null;
		ObjectInputStream o3 = null;
		FileInputStream m1 = null;
		FileInputStream m2 = null;
		FileInputStream m3 = null;
		
//Checking if the 3 memory files got any object saved and reads them
		while(!fileExist1 || !fileExist2 || !fileExist3)
		{
	//--- Checking memory slot 1-------------------------------------
			try {	
				m1 = new FileInputStream("m1");
				fileExist1 = true;
			}
			catch(IOException e) {
				FileOutputStream fos = new FileOutputStream("m1");
				ObjectOutputStream oos = new ObjectOutputStream(fos);
				oos.writeObject(slot1);
				oos.close();				
			}
	//--- Checking memory slot 2-------------------------------------		
			try {	
				m2 = new FileInputStream("m2");
				fileExist2 = true;
			}
			catch(IOException e) {
				FileOutputStream fos = new FileOutputStream("m2");
				ObjectOutputStream oos = new ObjectOutputStream(fos);
				oos.writeObject(slot2);
				oos.close();
			}
	//--- Checking memory slot 3-------------------------------------		
			try {	
				m3 = new FileInputStream("m3");
				fileExist3 = true;
			}
			catch(IOException e) {
				FileOutputStream fos = new FileOutputStream("m3");
				ObjectOutputStream oos = new ObjectOutputStream(fos);
				oos.writeObject(slot3);
				oos.close();
			}
		}
//--- Reading the objects and assigning them to memory slots----------				
	
		o1 = new ObjectInputStream(m1);
		o2 = new ObjectInputStream(m2);
		o3 = new ObjectInputStream(m3);
			
		try {
			slot1 = (MyGame) o1.readObject();
			slot2 = (MyGame) o2.readObject();
			slot3 = (MyGame) o3.readObject();
		}
		catch(ClassNotFoundException e)
		{
			System.out.println("Problem 1");
			e.printStackTrace();
		}
//--- Assigned objects to memory slots,so they can be closed now			
		o1.close();
		o2.close();
		o3.close();
		
//--- Assigning and returning Array of memory slots
		slots[0] = slot1;
		slots[1] = slot2;
		slots[2] = slot3;
		
		return slots;
	}
	
	@SuppressWarnings("resource")
	public static MyGame mainMenu(MyGame[] slots) // Shows menu and allows user to select memory slot to play on
	{
		MyGame slot1 = slots[0];
		MyGame slot2 = slots[1];
		MyGame slot3 = slots[2];
		
		String s1 = "Empty";
		String s2 = "Empty";
		String s3 = "Empty";
		
		boolean menu = true;
//--- Changing empty for hero name in case memory slot is not empty
		
		if(slot1 != null)
		{
			s1 = slot1.getHero().getName();
		}
		if(slot2 != null)
		{
			s2 = slot2.getHero().getName();
		}
		if(slot3 != null)
		{
			s3 = slot3.getHero().getName();
		}			
				
//--- While loop starts for the user to select in which memory slot to play
		while(menu)
		{	
			Scanner key = new Scanner(System.in);
			MyGame active;
			System.out.println("====== ELIHU GAME =====\n");
			System.out.println("Slot 1: " + s1 + "\n");
			System.out.println("Slot 2: " + s2 + "\n");
			System.out.println("Slot 3: " + s3 + "\n");
			System.out.println();

			
			String selection = key.nextLine();
			
			if(selection.equals("1"))
			{
				active = slot1;
				menu = false;
				return active;
			}
			else if(selection.equals("2"))
			{
				active = slot2;
				menu = false;
				return active;
			}
			else if(selection.equals("3"))
			{
				active = slot3;
				menu = false;
				return active;
			}
			else
			{
				System.out.println("Enter a valid slot (1,2 or 3)\n");
			}
		}
		return null;
		// End of while loop, proceed to actual game
	}
	
	@SuppressWarnings("resource")
	public static MyGame verifyGame(MyGame active)//Checking if user wants to overwrite current file or start a new adventure
	{
		Scanner key = new Scanner(System.in);
		//--- If memory is free
		if(active == null)
		{
			System.out.println("This slot is free, would you like to start a new game? (Y/N) ");
			String answer = key.nextLine();
			if(answer.equalsIgnoreCase("n"))
			{
				return null;
			}
			else if(answer.equalsIgnoreCase("y"))
			{
				active = newGame();
				return active;
			}
			else {
				System.out.println("Please enter a valid option");
				return null;
			}
		}
		//--- If there is a game already on that slot
		else{
			System.out.println("To continue press 'C'\nTo start a new adventure press 'N' \nTo go back to the menu press any other button" );
			String answer = key.nextLine();
			if(answer.equalsIgnoreCase("c"))
			{
				return active;
			}
			else if(answer.equalsIgnoreCase("n"))
			{
				System.out.println("Are you sure? You will overwrite the current saved game");
				System.out.println("Overwrite and continue (Y)");
				System.out.println("Back to main menu (Any other button)");
				String verify = key.nextLine();
				if(verify.equalsIgnoreCase("y"))
				{
					active = newGame();
					return active;
				}
				return null;
			}
			else {
				return null;
			}
		}
	}
		
	@SuppressWarnings("resource")
	public static MyGame newGame()//Creates a new session with the Hero name specified and returns it
	{
		MyGame session;
		Scanner key = new Scanner(System.in);
		System.out.println("Enter your Hero name:");
		String name = key.nextLine();
		try {
			session = new MyGame(name);
			return session;	
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Problem instantiating Items");
			return null;
		}
			
	}


}
