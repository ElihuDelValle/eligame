package eligame;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class Enemy implements java.io.Serializable{

	private static final long serialVersionUID = 1L;
	//Display attributes
	private String name;
	private int currentHp;
	//Real Stats
	private int level;
	private int maxHp;
	private int speedMax;
	private int speedCurrent;
	private int speed;
	private int attack;
	private int defence;
	private int accuracy;
	private int dodge;
	private List<Item> drops;
	//Others
	private int experience;
	private int money;
	
	public Enemy(String name) {
		
		this.name = name;
		speedMax = 100000;
		speedCurrent = 1;
		drops = new ArrayList<Item>();
		
		//Zone 1--------------------------------------------
		if(name.equalsIgnoreCase("baby goblin"))
		{
			level = 1;
			maxHp = 20;
			currentHp = maxHp;
			speed = 10;
			attack = 15;
			defence = 48;
			accuracy = 65;
			dodge = 0;
			experience = 15;
			money = 7;
		}
		else if(name.equalsIgnoreCase("baby wolf"))
		{
			level = 2;
			maxHp = 90;
			currentHp = maxHp;
			speed = 25;
			attack = 10;
			defence = 58;
			accuracy = 65;
			dodge = 0;
			experience = 34;
			money = 7;
		}
		else if(name.equalsIgnoreCase("baby bat"))
		{
			level = 3;
			maxHp = 100;
			currentHp = maxHp;
			speed = 57;
			attack = 40;
			defence = 115;
			accuracy = 85;
			dodge = 5;
			experience = 93;
			money = 7;
		}
		else if(name.equalsIgnoreCase("baby roo"))
		{
			level = 4;
			maxHp = 200;
			currentHp = maxHp;
			speed = 27;
			attack = 80;
			defence = 155;
			accuracy = 75;
			dodge = 1;
			experience = 77;
			money = 7;
		}		
		else if(name.equalsIgnoreCase("baby bear"))
		{
			level = 5;
			maxHp = 250;
			currentHp = maxHp;
			speed = 35;
			attack = 133;
			defence = 190;
			accuracy = 70;
			dodge = 1;
			experience = 146;
			money = 7;
		}
		//   Zone 2--------------------------------------------
		else if(name.equalsIgnoreCase("goblin"))
		{
			level = 6;
			maxHp = 470;
			currentHp = maxHp;
			speed = 48;
			attack = 165;
			defence = 180;
			accuracy = 71;
			dodge = 2;
			experience = 200;
			money = 100000;
		}
		else if(name.equalsIgnoreCase("bat"))
		{
			level = 6;
			maxHp = 350;
			currentHp = maxHp;
			speed = 95;
			attack = 140;
			defence = 160;
			accuracy = 75;
			dodge = 5;
			experience = 260;
			money = 100000;
		}
		else if(name.equalsIgnoreCase("wild roo"))
		{
			level = 7;
			maxHp = 430;
			currentHp = maxHp;
			speed = 81;
			attack = 200;
			defence = 200;
			accuracy = 78;
			dodge = 3;
			experience = 280;
			money = 100000;
		}
		else if(name.equalsIgnoreCase("wild wolf"))
		{
			level = 8;
			maxHp = 600;
			currentHp = maxHp;
			speed = 145;
			attack = 190;
			defence = 210;
			accuracy = 81;
			dodge = 2;
			experience = 284;
			money = 100000;
		}
		else if(name.equalsIgnoreCase("wild bear"))
		{
			level = 9;
			maxHp = 1220;
			currentHp = maxHp;
			speed = 74;
			attack = 300;
			defence = 270;
			accuracy = 76;
			dodge = 0;
			experience = 300;
			money = 100000;
		}
	//  Zone 3--------------------------------------------
		else if(name.equalsIgnoreCase("Scavenger"))
		{
			level = 11;
			maxHp = 985;
			currentHp = maxHp;
			speed = 165;
			attack = 340;
			defence = 350;
			accuracy = 75;
			dodge = 2;
			experience = 400;
			money = 100000;
		}
		else if(name.equalsIgnoreCase("baby golem"))
		{
			level = 12;
			maxHp = 2000;
			currentHp = maxHp;
			speed = 86;
			attack = 450;
			defence = 450;
			accuracy = 75;
			dodge = 0;
			experience = 450;
			money = 100000;
		}
		else if(name.equalsIgnoreCase("murloc"))
		{
			level = 13;
			maxHp = 1400;
			currentHp = maxHp;
			speed = 170;
			attack = 380;
			defence = 400;
			accuracy = 78;
			dodge = 3;
			experience = 470;
			money = 100000;
		}
		else if(name.equalsIgnoreCase("baby ogre"))
		{
			level = 12;
			maxHp = 1200;
			currentHp = maxHp;
			speed = 145;
			attack = 320;
			defence = 360;
			accuracy = 74;
			dodge = 1;
			experience = 430;
			money = 100000;
		}
		else if(name.equalsIgnoreCase("creeper"))
		{
			level = 15;
			maxHp = 1750;
			currentHp = maxHp;
			speed = 180;
			attack = 450;
			defence = 550;
			accuracy = 90;
			dodge = 5;
			experience = 550;
			money = 100000;
		}
		else if(name.equalsIgnoreCase("wild lion"))
		{
			level = 14;
			maxHp = 1700;
			currentHp = maxHp;
			speed = 195;
			attack = 455;
			defence = 480;
			accuracy = 85;
			dodge = 3;
			experience = 500;
			money = 100000;
		}
		//  Zone 4  --------------------------------------------
		else if(name.equalsIgnoreCase("Witch"))
		{
			level = 15;
			maxHp = 1950;
			currentHp = maxHp;
			speed = 100;
			attack = 750;
			defence = 450;
			accuracy = 80;
			dodge = 2;
			experience = 550;
			money = 100000;
		}
		else if(name.equalsIgnoreCase("warlord"))
		{
			level = 16;
			maxHp = 2050;
			currentHp = maxHp;
			speed = 140;
			attack = 700;
			defence = 550;
			accuracy = 85;
			dodge = 3;
			experience = 600;
			money = 100000;
		}
		else if(name.equalsIgnoreCase("t-rex"))
		{
			level = 22;
			maxHp = 5000;
			currentHp = maxHp;
			speed = 170;
			attack = 1000;
			defence = 1000;
			accuracy = 90;
			dodge = 5;
			experience = 1200;
			money = 100000;
		}
		else if(name.equalsIgnoreCase("drop bear"))
		{
			level = 17;
			maxHp = 2050;
			currentHp = maxHp;
			speed = 240;
			attack = 550;
			defence = 590;
			accuracy = 85;
			dodge = 2;
			experience = 712;
			money = 100000;
		}
		else if(name.equalsIgnoreCase("giant spider"))
		{
			level = 18;
			maxHp = 1000;
			currentHp = maxHp;
			speed = 340;
			attack = 550;
			defence = 700;
			accuracy = 85;
			dodge = 5;
			experience = 832;
			money = 100000;
		}
		else if(name.equalsIgnoreCase("velociraptor"))
		{
			level = 20;
			maxHp = 3000;
			currentHp = maxHp;
			speed = 495;
			attack = 705;
			defence = 850;
			accuracy = 90;
			dodge = 5;
			experience = 1000;
			money = 100000;
		}
		//  Zone 5  --------------------------------------------
		else if(name.equalsIgnoreCase("zombie"))
		{
			level = 21;
			maxHp = 3000;
			currentHp = maxHp;
			speed = 200;
			attack = 1200;
			defence = 1000;
			accuracy = 88;
			dodge = 1;
			experience = 1300;
			money = 100000;
		}
		else if(name.equalsIgnoreCase("dingo"))
		{
			level = 20;
			maxHp = 2500;
			currentHp = maxHp;
			speed = 590;
			attack = 685;
			defence = 700;
			accuracy = 99;
			dodge = 5;
			experience = 1123;
			money = 100000;
		}
		else if(name.equalsIgnoreCase("chimera"))
		{
			level = 23;
			maxHp = 4500;
			currentHp = maxHp;
			speed = 300;
			attack = 800;
			defence = 900;
			accuracy = 90;
			dodge = 2;
			experience = 1500;
			money = 100000;
		}
		else if(name.equalsIgnoreCase("heartless"))
		{
			level = 25;
			maxHp = 6000;
			currentHp = maxHp;
			speed = 320;
			attack = 900;
			defence = 1000;
			accuracy = 90;
			dodge = 2;
			experience = 2200;
			money = 100000;
		}
		else if(name.equalsIgnoreCase("nobody"))
		{
			level = 27;
			maxHp = 7000;
			currentHp = maxHp;
			speed = 370;
			attack = 970;
			defence = 1200;
			accuracy = 90;
			dodge = 3;
			experience = 2500;
			money = 100000;
		}
		else if(name.equalsIgnoreCase("unversed"))
		{
			level = 29;
			maxHp = 8000;
			currentHp = maxHp;
			speed = 420;
			attack = 1200;
			defence = 1300;
			accuracy = 90;
			dodge = 4;
			experience = 2700;
			money = 100000;
		}
		//  Zone 6  --------------------------------------------
		else if(name.equalsIgnoreCase("golem"))
		{
			level = 31;
			maxHp = 10000;
			currentHp = maxHp;
			speed = 350;
			attack = 1500;
			defence = 1600;
			accuracy = 90;
			dodge = 5;
			experience = 3000;
			money = 100000;
		}
		else if(name.equalsIgnoreCase("ogre"))
		{
			level = 32;
			maxHp = 12000;
			currentHp = maxHp;
			speed = 550;
			attack = 1200;
			defence = 1500;
			accuracy = 90;
			dodge = 5;
			experience = 3500;
			money = 100000;
		}
		else if(name.equalsIgnoreCase("gohma"))
		{
			level = 43;
			maxHp = 50000;
			currentHp = maxHp;
			speed = 800;
			attack = 1500;
			defence = 1000;
			accuracy = 100;
			dodge = 20;
			experience = 9000;
			money = 100000;
		}
		else if(name.equalsIgnoreCase("poltergeist"))
		{
			level = 35;
			maxHp = 9999;
			currentHp = maxHp;
			speed = 1000;
			attack = 1300;
			defence = 2000;
			accuracy = 100;
			dodge = 35;
			experience = 4000;
			money = 100000;
		}
		else if(name.equalsIgnoreCase("dire wolf"))
		{
			level = 38;
			maxHp = 15000;
			currentHp = maxHp;
			speed = 1200;
			attack = 1350;
			defence = 2000;
			accuracy = 100;
			dodge = 20;
			experience = 5000;
			money = 100000;
		}
		else if(name.equalsIgnoreCase("white walker"))
		{
			level = 40;
			maxHp = 13000;
			currentHp = maxHp;
			speed = 500;
			attack = 1750;
			defence = 2300;
			accuracy = 95;
			dodge = 10;
			experience = 6000;
			money = 100000;
		}
		//  Zone 7  --------------------------------------------
		else if(name.equalsIgnoreCase("deathclaw"))
		{
			level = 53;
			maxHp = 27000;
			currentHp = maxHp;
			speed = 900;
			attack = 2800;
			defence = 3500;
			accuracy = 100;
			dodge = 50;
			experience = 17000;
			money = 100000;
		}
		else if(name.equalsIgnoreCase("straga"))
		{
			level = 59;
			maxHp = 27000;
			currentHp = maxHp;
			speed = 1500;
			attack = 3300;
			defence = 4000;
			accuracy = 110;
			dodge = 41;
			experience = 22000;
			money = 100000;
		}
		else if(name.equalsIgnoreCase("hades"))
		{
			level = 59;
			maxHp = 26000;
			currentHp = maxHp;
			speed = 900;
			attack = 3600;
			defence = 4500;
			accuracy = 110;
			dodge = 42;
			experience = 22000;
			money = 100000;
		}
		else if(name.equalsIgnoreCase("reaper"))
		{
			level = 50;
			maxHp = 25000;
			currentHp = maxHp;
			speed = 1000;
			attack = 2200;
			defence = 3000;
			accuracy = 112;
			dodge = 40;
			experience = 15000;
			money = 100000;
		}
		else if(name.equalsIgnoreCase("sith"))
		{
			level = 57;
			maxHp = 30000;
			currentHp = maxHp;
			speed = 1500;
			attack = 3000;
			defence = 3700;
			accuracy = 110;
			dodge = 60;
			experience = 20000;
			money = 100000;
		}
		//  Zone 8  --------------------------------------------
		else if(name.equalsIgnoreCase("dementor"))
		{
			level = 60;
			maxHp = 40000;
			currentHp = maxHp;
			speed = 1000;
			attack = 3500;
			defence = 4000;
			accuracy = 110;
			dodge = 30;
			experience = 22222;
			money = 100000;
		}
		else if(name.equalsIgnoreCase("weeping angel"))
		{
			level = 61;
			maxHp = 36000;
			currentHp = maxHp;
			speed = 1300;
			attack = 3300;
			defence = 4000;
			accuracy = 90;
			dodge = 23;
			experience = 23232;
			money = 100000;
		}
		else if(name.equalsIgnoreCase("hydra"))
		{
			level = 62;
			maxHp = 38000;
			currentHp = maxHp;
			speed = 800;
			attack = 3300;
			defence = 4500;
			accuracy = 100;
			dodge = 20;
			experience = 24000;
			money = 100000;
		}
		else if(name.equalsIgnoreCase("pyramid head"))
		{
			level = 65;
			maxHp = 36666;
			currentHp = maxHp;
			speed = 200;
			attack = 9666;
			defence = 5000;
			accuracy = 190;
			dodge = 0;
			experience = 26666;
			money = 100000;
		}
		else if(name.equalsIgnoreCase("ganon"))
		{
			level = 66;
			maxHp = 40000;
			currentHp = maxHp;
			speed = 1800;
			attack = 4200;
			defence = 5000;
			accuracy = 110;
			dodge = 75;
			experience = 28000;
			money = 100000;
		}
		else if(name.equalsIgnoreCase("nemesis"))
		{
			level = 68;
			maxHp = 44321;
			currentHp = maxHp;
			speed = 2500;
			attack = 5000;
			defence = 6000;
			accuracy = 110;
			dodge = 80;
			experience = 30000;
			money = 100000;
		}
		else if(name.equalsIgnoreCase("darth maul"))
		{
			level = 70;
			maxHp = 60000;
			currentHp = maxHp;
			speed = 2500;
			attack = 4500;
			defence = 6500;
			accuracy = 110;
			dodge = 90;
			experience = 35000;
			money = 100000;
		}
		//  Dungeon 1 --------------------------------------------
		else if(name.equalsIgnoreCase("noir snake"))
		{
			level = 14;
			maxHp = 5000;
			currentHp = maxHp;
			speed = 160;
			attack = 530;
			defence = 500;
			accuracy = 80;
			dodge = 3;
			experience = 1100;
			money = 100000;
		}
		else if(name.equalsIgnoreCase("gargoyle"))
		{
			level = 12;
			maxHp = 3500;
			currentHp = maxHp;
			speed = 145;
			attack = 400;
			defence = 400;
			accuracy = 74;
			dodge = 1;
			experience = 700;
			money = 100000;
		}
		else if(name.equalsIgnoreCase("elite"))
		{
			level = 15;
			maxHp = 6000;
			currentHp = maxHp;
			speed = 220;
			attack = 700;
			defence = 700;
			accuracy = 80;
			dodge = 3;
			experience = 1400;
			money = 100000;
		}
		else if(name.equalsIgnoreCase("balverine"))
		{
			level = 15;
			maxHp = 6000;
			currentHp = maxHp;
			speed = 300;
			attack = 650;
			defence = 620;
			accuracy = 80;
			dodge = 10;
			experience = 1400;
			money = 100000;
		}
		else if(name.equalsIgnoreCase("crawler"))
		{
			level = 17;
			maxHp = 15000;
			currentHp = maxHp;
			speed = 350;
			attack = 699;
			defence = 899;
			accuracy = 150;
			dodge = 3;
			experience = 20000;
			money = 100000;
		}
		//  Dungeon 2 --------------------------------------------
		else if(name.equalsIgnoreCase("cerberus"))
		{
			level = 29;
			maxHp = 25000;
			currentHp = maxHp;
			speed = 620;
			attack = 1890;
			defence = 1900;
			accuracy = 99;
			dodge = 12;
			experience = 10000;
			money = 100000;
		}
		else if(name.equalsIgnoreCase("leviathan"))
		{
			level = 27;
			maxHp = 20000;
			currentHp = maxHp;
			speed = 550;
			attack = 1500;
			defence = 1750;
			accuracy = 99;
			dodge = 10;
			experience = 7000;
			money = 100000;
		}
		else if(name.equalsIgnoreCase("ifrit"))
		{
			level = 28;
			maxHp = 22000;
			currentHp = maxHp;
			speed = 570;
			attack = 1650;
			defence = 1800;
			accuracy = 99;
			dodge = 10;
			experience = 8000;
			money = 100000;
		}
		else if(name.equalsIgnoreCase("shiva"))
		{
			level = 26;
			maxHp = 18000;
			currentHp = maxHp;
			speed = 470;
			attack = 1380;
			defence = 1500;
			accuracy = 99;
			dodge = 10;
			experience = 6000;
			money = 100000;
		}
		else if(name.equalsIgnoreCase("quetzacotl"))
		{
			level = 24;
			maxHp = 15000;
			currentHp = maxHp;
			speed = 450;
			attack = 1200;
			defence = 1400;
			accuracy = 99;
			dodge = 10;
			experience = 5000;
			money = 100000;
		}
		else if(name.equalsIgnoreCase("Iznatron"))
		{
			level = 30;
			maxHp = 75000;
			currentHp = maxHp;
			speed = 722;
			attack = 2222;
			defence = 2222;
			accuracy = 222;
			dodge = 3;
			experience = 122222;
			money = 100000;
		}
	//  Dungeon 3 --------------------------------------------
			else if(name.equalsIgnoreCase("devil's guardian"))
			{
				level = 40;
				maxHp = 50000;
				currentHp = maxHp;
				speed = 900;
				attack = 2300;
				defence = 3000;
				accuracy = 99;
				dodge = 28;
				experience = 20000;
				money = 100000;
			}
			else if(name.equalsIgnoreCase("dark knight"))
			{
				level = 45;
				maxHp = 60000;
				currentHp = maxHp;
				speed = 1100;
				attack = 2600;
				defence = 3700;
				accuracy = 99;
				dodge = 30;
				experience = 23000;
				money = 100000;
			}
			else if(name.equalsIgnoreCase("viserion"))
			{
				level = 48;
				maxHp = 70000;
				currentHp = maxHp;
				speed = 1300;
				attack = 3200;
				defence = 4000;
				accuracy = 99;
				dodge = 50;
				experience = 26000;
				money = 100000;
			}
			else if(name.equalsIgnoreCase("rhaegal"))
			{
				level = 49;
				maxHp = 82000;
				currentHp = maxHp;
				speed = 1400;
				attack = 3400;
				defence = 4200;
				accuracy = 99;
				dodge = 50;
				experience = 28000;
				money = 100000;
			}
			else if(name.equalsIgnoreCase("drogon"))
			{
				level = 50;
				maxHp = 99999;
				currentHp = maxHp;
				speed = 1500;
				attack = 3600;
				defence = 4500;
				accuracy = 110;
				dodge = 60;
				experience = 30000;
				money = 100000;
			}
			else if(name.equalsIgnoreCase("night king"))
			{
				level = 55;
				maxHp = 298654;
				currentHp = maxHp;
				speed = 1600;
				attack = 4000;
				defence = 5300;
				accuracy = 222;
				dodge = 50;
				experience = 500000;
				money = 100000;
			}
		//  Dungeon 4 --------------------------------------------
			else if(name.equalsIgnoreCase("black dragon"))
			{
				level = 70;
				maxHp = 200000;
				currentHp = maxHp;
				speed = 2000;
				attack = 5500;
				defence = 7000;
				accuracy = 110;
				dodge = 99;
				experience = 100000;
				money = 100000;
			}
			else if(name.equalsIgnoreCase("daedra"))
			{
				level = 73;
				maxHp = 250000;
				currentHp = maxHp;
				speed = 2300;
				attack = 5800;
				defence = 7500;
				accuracy = 110;
				dodge = 99;
				experience = 130000;
				money = 100000;
			}
			else if(name.equalsIgnoreCase("diablo"))
			{
				level = 77;
				maxHp = 288000;
				currentHp = maxHp;
				speed = 2500;
				attack = 6200;
				defence = 8300;
				accuracy = 99;
				dodge = 100;
				experience = 200000;
				money = 100000;
			}
			else if(name.equalsIgnoreCase("ultima weapon"))
			{
				level = 81;
				maxHp = 320000;
				currentHp = maxHp;
				speed = 2810;
				attack = 6666;
				defence = 8888;
				accuracy = 99;
				dodge = 110;
				experience = 250000;
				money = 100000;
			}
			else if(name.equalsIgnoreCase("darth vader"))
			{
				level = 83;
				maxHp = 350000;
				currentHp = maxHp;
				speed = 2000;
				attack = 8000;
				defence = 9999;
				accuracy = 150;
				dodge = 0;
				experience = 500000;
				money = 100000;
			}
			else if(name.equalsIgnoreCase("sephirot"))
			{
				level = 85;
				maxHp = 450000;
				currentHp = maxHp;
				speed = 3500;
				attack = 8500;
				defence = 11111;
				accuracy = 160;
				dodge = 110;
				experience = 1000000;
				money = 100000;
			}
			else if(name.equalsIgnoreCase("shadow"))
			{
				level = 90;
				maxHp = 1000000;
				currentHp = maxHp;
				speed = 5000;
				attack = 12345;
				defence = 18000;
				accuracy = 255;
				dodge = 120;
				experience = 5000000;
				money = 100000;
			}
	}


	// Battle Method --------------------
	public void attackReceived(Hero hero)
	{
		int heroLevel = hero.getLevel();
		int levelDif = level - heroLevel;
		int heroStr = hero.getAttack();
		int heroAcc = hero.getAccuracy();
		double chance = (heroAcc/100.0)-(dodge/100.0);
		double randomNum = Math.random();
		if(chance<randomNum)
		{
			System.out.println("===================================");
			System.out.println(name + " has dodged your attack");
			System.out.println("===================================");
			return;
		}
		int minimumDamage = heroLevel;
		if(levelDif>10)
		{
			minimumDamage = 10;
		}
		if(levelDif>50)
		{
			minimumDamage = 1;
		}
		double defEffectiveness = (25 * Math.random())/100;
		double tempDefence = defence - (defence*defEffectiveness);
		double attackEffectiveness = (15 * Math.random())/100;
		double tempAttack = heroStr - (heroStr*attackEffectiveness);
		int damage = (int)tempAttack - (int)tempDefence;
		if(levelDif>0)
		{
			damage = (int)(damage / (1+((levelDif*levelDif)/1000.0)));
		}
		else if(levelDif<0)
		{
			damage = (int)(damage * (1+((levelDif*levelDif)/1000.0)));
		}
		if(damage <= 0)
		{
			damage = 0;
		}
		currentHp -= damage + minimumDamage;
		System.out.println("===================================");
		System.out.println("You have attacked " + name + "(" + (damage+minimumDamage) + " Damage)");
		System.out.println("===================================");
		}	
		
	// Refreshers -------------------------------
	public void refresh()
	{
		currentHp = maxHp;
		speedCurrent = 0;
	}
		
	public void restartTime()
	{
		speedCurrent = 0;
	}
		
	public void time()
	{
		if(speedCurrent<speedMax)
		{
			speedCurrent += speed;
		}
	}
		
	public void modifyChance(Item itemToModify,double newChance)
	{
		Item itemTemp;
		for(int index=0; index<drops.size(); index++)
		{
			itemTemp = drops.get(index);
			if(itemTemp.getName().equalsIgnoreCase(itemToModify.getName()))
			{
				itemTemp.setChance(newChance);
				return;
			}
		}
	}
	
	//------------------- Getters ----------------------
	public String getName()
	{
		return name;
	}
	
	public int getMaxHp()
	{
		return maxHp;
	}
	
	public int getCurrentHp()
	{
		return currentHp;
	}
	
	public int getSpeedMax()
	{
		return speedMax;
	}
	
	public int getLevel()
	{
		return level;
	}
	
	public int getSpeedCurrent()
	{
		return speedCurrent;
	}
	
	public int getSpeed()
	{
		return speed;
	}	
	
	public int getAttack()
	{
		return attack;
	}
	
	public int getDefence()
	{
		return defence;
	}
	
	public int getAccuracy()
	{
		return accuracy;
	}
	
	public int getDodge()
	{
		return dodge;
	}
	
	public int getExperience()
	{
		return experience;
	}
	
	public int getMoney()
	{
		return money;
	}

	public void addDrop(Item item,double chance)
	{
		try {
			ItemsHero itemTemp = new ItemsHero(item);
			itemTemp.setChance(chance);
			drops.add(itemTemp);
			return;
		}catch(Exception e) {}
		try {
			Weapon itemTemp = new Weapon(item.getName());
			itemTemp.setChance(chance);
			drops.add(itemTemp);
			return;
		}catch(Exception e) {}
		try {
			Armour itemTemp = new Armour(item.getName());
			itemTemp.setChance(chance);
			drops.add(itemTemp);
			return;
		}catch(Exception e) {}
	}
	
	public List<Item> getDrops()
	{
		return drops;
	}
	
	// Representation ---------------------------------------
	public String toString()
	{
		DecimalFormat formatter = new DecimalFormat("#0.00");
		String stringDrops = "";
		for(int index=0; index<drops.size(); index++)
		{
			double chanceTemp = (drops.get(index).getChance()*100);
			String chanceTempFinal = formatter.format(chanceTemp);
			stringDrops += drops.get(index).getName() + ": " + chanceTempFinal + "%\n";
		}
		String s = name + "\n" +
				"HP: (" + currentHp + "|" + maxHp + ")\n" +
				"Att: " + attack + "\n" +
				"Def: " + defence + "\n" + 
				"Spd: " + speed + "\n" +
				"Dod: " + dodge + "\n" + 
				"Acc: " + accuracy + "\n" + 
				"------ Drops -------\n" + stringDrops;
		
		return s;
	}

}
