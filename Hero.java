package eligame;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Hero implements java.io.Serializable{

	private static final long serialVersionUID = 1L;
	//Display attributes
	private String name;
	private int level;
	private int currentHp;
	//Basic attributes
	private int strength;
	private int vitality;
	private int agility;
	//Real Stats
	private int maxHp;
	private int speedMax;
	private int speedCurrent;
	private int speed;
	private int attack;
	private int defence;
	private int accuracy;
	private int dodge;
	//Others
	private int statPoints;
	private int experience;
	private int experienceToNextLvl;
	private int experienceRemaining;
	private int money;
	private Weapon weaponEquipped;
	private Armour armourEquipped;
	private List<Weapon> weaponsOwned;
	private List<Armour> armoursOwned;
	private List<ItemsHero> items;
	
	public Hero(String name)
	{
		//Display attributes
		this.name = name;
		weaponEquipped = null;
		armourEquipped = null;
		level = 1;
		statPoints = 3;
		//Basic attributes
		strength = 1;
		vitality = 1;
		agility = 1;
		//Others
		experience = 0;
		experienceToNextLvl = 100;
		money = 0;
		//Real Stats
		speedMax = 100000;
		speedCurrent = 1;
		calculateStats();//Calculate stats	
		currentHp = maxHp;
		experienceRemaining = experienceToNextLvl-experience;
		//Instantiate lists
		items = new ArrayList<ItemsHero>();
		armoursOwned = new ArrayList<Armour>();
		weaponsOwned = new ArrayList<Weapon>();
	}
	
	//------------- Stats Methods -------------------------------------------------------------------------------------------------------------
	public void calculateStats()//Calculates spd,atk,def,acc,dodge,Hp from the basic attributes and lvl (Does not allow negative values)
	{
		speed = (int) ((level*1.5) + (agility * 6.5)*(1+(level/33.0)) );
		attack = (int) ((level*7.5)+ ( (strength * 20.3)*(1.0+(level/33.0)) ));
		defence = (int)((level*7.5)+ ( (vitality * 12.7)*(1.0+(level/33.0)) ));		
		accuracy = (int)((1.0+(agility/200.0) )*( 55.0 * (1.0+(level/63.26)) ))+((int)(agility/9.0));
		dodge = (int)( 1.0 + ((agility/6.0) + (level/6.5)) );
		maxHp = (int)( 32.0 * (vitality) * (1+(level/33.0)) ) + (level);	
		if(weaponEquipped != null)
		{
			attack += weaponEquipped.getAttackBonus();
		}
		if(armourEquipped != null)
		{
			defence += armourEquipped.getDefenceBonus();
		}
		if(speed<1) {
			speed = 1;
		}
		if(attack<1) {
			attack = 1;
		}
		if(defence<1) {
			defence = 1;
		}
		if(speed<1) {
			speed = 1;
		}
		if(accuracy<1) {
			speed = 1;
		}
		if(dodge<1) {
			speed = 1;
		}
		if(maxHp<1) {
			maxHp = 1;
		}
		if(accuracy>200)
		{
			accuracy = 200;
		}
		if(currentHp>maxHp)
		{
			currentHp = maxHp;
		}
	}
	
	public void levelUp()//Loops until no more lvl ups can be done and calculates next experience to lvl up
	{
		while(experienceRemaining<=0)
		{
			if(level == 100)
			{
				experienceRemaining = 0;
				return;
			}
			System.out.println("Congratulations! You have leveled up!!");
			experience -= experienceToNextLvl;
			level += 1;
			statPoints += 3;
			if((level%10) == 0)
			{
				statPoints += 5;
			}
			if((level%50) == 0)
			{
				statPoints += 7;
			}
			if((level%100) == 0)
			{
				statPoints += 20;
			}
			calculateStats();
			if(level == 100)
			{
				experienceRemaining = 0;
				return;
			}
			double bigNum = ((Math.pow(1.5, level))/(level));
			double bigNum2 = Math.cbrt(bigNum);
			double exp = ((level*455*level)+ (3*level * (bigNum2)))/10;
			exp = exp * 2.2;
			experienceToNextLvl = (int)exp;
			experienceRemaining = experienceToNextLvl - experience;
		}
	}
	
	//------------Battle Methods -----------------------------------------------------------------------------------------------------------
	public void attackReceived(Enemy enemy)//To calculate damage
	{
		int enemyLevel = enemy.getLevel();
		int levelDif = level - enemyLevel;
		int enemyStr = enemy.getAttack();
		int enemyAcc = enemy.getAccuracy();
		double chance = (enemyAcc/100.0)-(dodge/100.0);//Accuracy - dodge = chance
		double randomNum = Math.random();
		if(chance<randomNum)//Dodging
		{
			System.out.println("-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-");
			System.out.println("You have dodged the attack");
			System.out.println("-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-");
			return;
		}
		int minimumDamage = enemyLevel;
		if(levelDif>50)
		{
			minimumDamage = 1;
		}
		else if(levelDif>10)
		{
			minimumDamage = 10;
		}
		double defEffectiveness = (25 * Math.random())/100;//Randomly defence effectiveness reduces up to 25%
		double tempDefence = defence - (defence*defEffectiveness);
		double attackEffectiveness = (15 * Math.random())/100;//Randomly attack effectiveness reduces up to 15%
		double tempAttack = enemyStr - (enemyStr*attackEffectiveness);
		int damage = (int)tempAttack - (int)tempDefence;
		if(levelDif>0)//If Higher level than attacker, attack is reduced a percentage
		{
			damage = (int)(damage / (1+((levelDif*levelDif)/1000.0)));
		}
		else if(levelDif<0)//If Lower level than attacker, attack is increased a percentage
		{
			damage = (int)(damage * (1+((levelDif*levelDif)/1000.0)));
		}
		if(damage <= 0)
		{
			damage = 0;
		}
		currentHp -= damage + minimumDamage;
		System.out.println("***********************************");
		System.out.println(enemy.getName() + " has attacked you (" + (damage+minimumDamage) + " Damage) ");
		System.out.println("***********************************");
		return;	
	}

	public void restartTime()//Re-starts turn
	{
		speedCurrent = 0;
	}
	
	public void time()//Increases time
	{
		if(speedCurrent<speedMax)
		{
			speedCurrent += speed;
		}
	}

	@SuppressWarnings("resource")
	public boolean itemsBattle()//Shows list of items to be used in battle
	{
		int x = 0;
		boolean validSelection = false;
		boolean empty = false;
		boolean listEmpty = true;
		try {//If empty listEmpty is true
			items.get(0);
			listEmpty = false;
		}catch(Exception e) {}
		if(!listEmpty)// If not null load items
		{		
			System.out.println("======== Items =======\n");
			while(!empty)
			{
				try {//Loads all items in list
					ItemsHero tempItem = items.get(x);
					System.out.println(x+1 + ") " + tempItem.getName() + " (" + tempItem.getQuantity() + ")");
					tempItem = items.get(x+1);
				}
				catch(Exception e)
				{
					empty = true;
				}
				x++;
			}
			System.out.println("B) Back\n");
			System.out.println("===============");
			Scanner key = new Scanner(System.in);
			while(!validSelection)
			{
				String selection = key.nextLine();
				if(selection.equalsIgnoreCase("B"))//Exits item menu
				{
					return true;
				}
				else {
					try {
						int index = Integer.parseInt(selection);//Checks correct input
						if(index>x)
						{
							System.out.println("Enter a valid number");
							return true;
						}
						else{
							validSelection = true;
							ItemsHero itemSelected = items.get(index-1);//Loads item selected
							if(itemSelected.getQuantity() <= 0)
							{
								validSelection = false;
							}
							else {
								System.out.println("-----------------------------------");
								System.out.println("You have used " + itemSelected.getName());
								itemSelected.increaseQuantity(-1);//Decreases quantity of item
								if(itemSelected.getType().equalsIgnoreCase("hp"))//If type HP, heals HP
								{
									double amountHealed;
									double heal = itemSelected.getPoints();
									if(heal>(1.1))
									{
										int healInt = (int)heal;
										amountHealed = increaseCurrentHp(healInt);//Heals amount
									}
									else {
										amountHealed = increaseCurrentHp(heal);//Heals percentage
									}
									System.out.println("You have healed " + ((int)amountHealed));
								}
								if(itemSelected.getQuantity() == 0)
								{
									items.remove(index-1);//If reaches 0, eliminates item from list
								}
							}
						}
						return false;
					}
					catch(Exception e)
					{
						System.out.println("Incorrect selection");
						return true;
					}
				}
			}
		}
		else {
			System.out.println("You have no items");
			return true;
		}
		System.out.println("Leak in battleItems");
		return true;
	}
	
	//-------------  Equipments and Items  ---------------------------------------------------------------------------------------------------
	public void equipWeapon(Weapon weaponTemp)//Equips weapon and modifies stats/ Unequips previous weapon equipped
	{
		boolean allGood = false;
		while(!allGood)
		{
			if(weaponEquipped == null)
			{
				weaponEquipped = weaponTemp;
				agility += weaponEquipped.getAgilityBonus();
				strength += weaponEquipped.getStrengthBonus();
				vitality += weaponEquipped.getVitalityBonus();
				calculateStats();
				allGood = true;
			}
			else {
				unequipWeapon();
			}
		}
	}
	
	public void equipArmour(Armour armourTemp)//Equips armour and modifies stats/ Unequips previous armour equipped
	{
		boolean allGood = false;
		while(!allGood)
		{
			if(armourEquipped == null)
			{
				armourEquipped = armourTemp;
				agility += armourEquipped.getAgilityBonus();
				strength += armourEquipped.getStrengthBonus();
				vitality += armourEquipped.getVitalityBonus();
				calculateStats();
				allGood = true;
			}
			else {
				unequipArmour();
			}
		}
	}
	
	public void unequipWeapon()//Unequips weapon and calculate stats
	{
		if(weaponEquipped == null)
		{
			return;
		}
		else{
			int tempAttack = weaponEquipped.getAttackBonus();
			int tempAgi = weaponEquipped.getAgilityBonus();
			int tempVit = weaponEquipped.getVitalityBonus();
			int tempStr = weaponEquipped.getStrengthBonus();
			System.out.println("You have unequipped " + weaponEquipped.getName());
			weaponEquipped = null;
			attack -= tempAttack;
			agility -= tempAgi;
			vitality -= tempVit;
			strength -= tempStr;
			calculateStats();				
		}	
	}
	
	public void unequipArmour()//Unequips armour and calculate stats
	{
		if(armourEquipped == null)
		{
			return;
		}
		else{
			int tempDefence = armourEquipped.getDefenceBonus();
			int tempAgi = armourEquipped.getAgilityBonus();
			int tempVit = armourEquipped.getVitalityBonus();
			int tempStr = armourEquipped.getStrengthBonus();
			System.out.println("You have unequipped " + armourEquipped.getName());
			armourEquipped = null;
			attack -= tempDefence;
			agility -= tempAgi;
			vitality -= tempVit;
			strength -= tempStr;
			calculateStats();				
		}	
	}
	
	public void addWeapon(Weapon weapon)//Adds weapon to list of owned weapons
	{
		weaponsOwned.add(weapon);
	}
	
	public void addArmour(Armour armour)//Adds armour to list of owned armours
	{
		armoursOwned.add(armour);
	}
	
	public void addItem(Item item)//Takes item, casts it to ItemHero and adds it to the list of owned items
	{
		ItemsHero itemTemp;
		try {
			itemTemp = new ItemsHero(item);
			items.add(itemTemp);
		} catch (Exception e) {
			System.out.println("Fix this, Hero Class");
		}
		
	}
	
	//---------------- Utilities  -------------------------------------------------------------------------------------------------
	public boolean showItems()//Displays all owned items by hero and their quantity, returns true if not empty
	{
		try {
			items.get(0);
			for(int index = 0; index<items.size(); index++)
			{
				if(items.get(index) != null)
				{
					ItemsHero itemTemp = items.get(index);
					System.out.println((index+1) + ") " + itemTemp.getName() + " (" + itemTemp.getQuantity() + ")");
				}
				else {
					return true;
				}
			}	
		}
		catch(Exception e)
		{
			System.out.println("You have no items");
			MyGame.textToScreen("      ");
			return false;
		}	
		return true;
	}
	
	public Item findItem(Item item)//Looks for item passed and returns it if exists
	{
		String itemName = item.getName();
		for(int index = 0; index<items.size(); index++)
		{
			if(items.get(index) != null)
			{
				if(items.get(index).getName().equalsIgnoreCase(itemName))
				{
					return items.get(index);
				}
			}
		}
		return null;
	}
	
	public Weapon findWeapon(Item item)//Looks for item passed and returns it if exists
	{
		String itemName = item.getName();
		for(int index = 0; index<weaponsOwned.size(); index++)
		{
			if(weaponsOwned.get(index) != null)
			{
				if(weaponsOwned.get(index).getName().equalsIgnoreCase(itemName))
				{
					return weaponsOwned.get(index);
				}
			}
		}
		return null;
	}
	
	public Armour findArmour(Item item)//Looks for item passed and returns it if exists
	{
		String itemName = item.getName();
		for(int index = 0; index<armoursOwned.size(); index++)
		{
			if(armoursOwned.get(index) != null)
			{
				if(armoursOwned.get(index).getName().equalsIgnoreCase(itemName))
				{
					return armoursOwned.get(index);
				}
			}
		}
		return null;
	}
	
	//-------------------- Getters --------------------------------------------------------------------------------------------------
	public String getName()
	{
		return name;
	}	

	public int getLevel()
	{
		return level;
	}
	
	public int getMaxHp()
	{
		return maxHp;
	}	
	
	public int getCurrentHp()
	{
		return currentHp;
	}
	
	public int getStrength()
	{
		return strength;
	}
	
	public int getVitality()
	{
		return vitality;
	}
	
	public int getAgility()
	{
		return agility;
	}
	
	public int getSpeedMax()
	{
		return speedMax;
	}
	
	public int getSpeedCurrent()
	{
		return speedCurrent;
	}
	
	public int getSpeed()
	{
		return speed;
	}
	
	public int getAttack()
	{
		return attack;
	}
	
	public int getDefence()
	{
		return defence;
	}	

	public int getAccuracy()
	{
		return accuracy;
	}
	
	public int getDodge()
	{
		return dodge;
	}
	
	public int getStatPoints()
	{
		return statPoints;
	}
	
	public int getMoney()
	{
		return money;
	}	
	
	public int getExperience()
	{
		return experience;
	}	
	
	public int getExperienceToNextLevel()
	{
		return experienceToNextLvl;
	}
	
	public int getExperienceRemaining()
	{
		return experienceRemaining;
	}

	public Weapon getWeaponEquipped()
	{
		return weaponEquipped;
	}
	
	public Armour getArmourEquipped()
	{
		return armourEquipped;
	}
	
	public List<Armour> getArmoursOwned()
	{
		return armoursOwned;
	}
	
	public List<ItemsHero> getItems()
	{
		return items;
	}
	
	public List<Weapon> getWeaponsOwned()
	{
		return weaponsOwned;
	}
		
	//-----------------  Modifiers ------------------------------------------------------------------------------------
	public void increaseMaxHp(int hp)//Increases max hp directly by amount passed
	{
		maxHp += hp;
	}
	
	public int increaseCurrentHp(double percentage)//Increases current hp by percentage passed
	{
		if(currentHp != maxHp)
		{
			int heal = (int)(maxHp*(percentage));
			int newHp = currentHp + heal;
			if (newHp > maxHp)
			{
				heal = maxHp - currentHp;
				currentHp = maxHp;
				return heal;
			}
			currentHp = newHp;
			return heal;
		}
		return 0;
	}
	
	public int increaseCurrentHp(int heal)//Increases current hp by direct amount
	{
		if(currentHp != maxHp)
		{
			int newHp = currentHp + heal;
			if (newHp > maxHp)
			{
				heal = maxHp - currentHp;
				currentHp = maxHp;
				return heal;
			}
			currentHp = newHp;
			return heal;
		}
		return 0;
	}
	
	public void setCurrentHp(int hp)//Sets current hp
	{
		if(hp>maxHp)
		{
			currentHp = maxHp;
		}
		currentHp = hp;
	}
	
	public void increaseStr(int str)
	{
		strength += str;
	}
	
	public void increaseAgi(int agi)
	{
		agility += agi;
	}
	
	public void increaseVit(int vit)
	{
		vitality += vit;
	}
	
	public void increaseAttack(int att)
	{
		attack += att;
	}
	
	public void increaseDefence(int def)
	{
		defence += def;
	}
	
	public void increaseSpeed(int spd)
	{
		speed += spd;
	}
	
	public void increaseAccuracy(int acc)
	{
		accuracy += acc;
	}
	
	public void increaseDodge(int dod)
	{
		dodge += dod;
	}	
	
	public void increaseStatPoints(int stats)
	{
		statPoints += stats;
	}	
	
	public void increaseMoney(int mon)
	{
		money += mon;
	}	
	
	public void increaseExperience(int exp)
	{
		if(level == 100)
		{
			experienceRemaining = 0;
			return;
		}
		experience += exp;
		experienceRemaining -= exp;
		if(experience>experienceToNextLvl)
		{
			levelUp();
		}
	}	
	
	//-------------------------  Representation ---------------------------------------------------------------------------
	public String toString()
	{
		String weaponName = "None";
		String armourName = "None";
		if(weaponEquipped != null)
		{
			weaponName = weaponEquipped.getName();
		} 
		if(armourEquipped != null)
		{
			armourName = armourEquipped.getName();
		} 
		String experienceRemainingTemp =  NumberFormat.getIntegerInstance().format(experienceRemaining);
		String space2 = "---------------------------------------------\n";
		String toNextLvl2 = "To next Lv:" + experienceRemainingTemp + " Exp";
		String level2 = "Level: " + level;  
		String statPoints2 = "Stat Points:" + statPoints;
		String weapon2 = "Weapon: " + weaponName;
		String armour2 = "Armour: " + armourName;
		String hp2 = "HP: (" + currentHp + "|" + maxHp + ")";
		String str2 = "Strength: " + strength;
		String agi2 = "Agility:  " + agility;
		String money2 = "Money: " + "$" + money + ".00";
		String vit2 = "Vitality: " + vitality;
		String def2 = "Defence:" + defence;
		String att2 = "Attack:" + attack;
		String spd2 = "Speed:" + speed;
		String dod2 = "Dodge:" + dodge + "%";
		String acc2 = "Accuracy:" + accuracy + "%";
	
		name = String.format("%-25s", name);  
		statPoints2 = String.format("%-25s", statPoints2);
		weapon2 = String.format("%-25s", weapon2);
		hp2 = String.format("%-25s", hp2);
		str2 = String.format("%-25s", str2);
		def2 = String.format("%-25s", def2);
		att2 = String.format("%-25s", att2);
			
		String finalString2 = "\n" +  name  + level2 + "\n" +
				space2 +
				statPoints2  + toNextLvl2 + "\n" +
				space2 +
				weapon2  + armour2 + "\n" +
				space2 +
				str2  + money2 + "\n" +
				vit2 + "\n" +
				agi2 + "\n" +
				space2 + 
				hp2  + spd2 + "\n" + 
				att2  + dod2 + "\n" + 
				def2  + acc2 + "\n";
						
		return finalString2;
	}

}
