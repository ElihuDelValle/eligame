package eligame;

import java.io.FileOutputStream;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.*;

public class MyGame implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private Hero hero;
	private List<Enemy> enemies;
	private List<Weapon> weapons;
	private List<Armour> armours;
	private List<Item> items;
	private List<Temple> temples;
	private int episode;
	private WorldMap map;
	
	public MyGame(String heroName) throws Exception
	{
		map = new WorldMap();
		episode = 1;
		enemies = new ArrayList<Enemy>();
		items = new ArrayList<Item>();
		armours = new ArrayList<Armour>();
		weapons = new ArrayList<Weapon>();
		temples = new ArrayList<Temple>();
		hero = new Hero(heroName);
		
		//Adding items
		/*
		Weapon woodenStick = new Weapon("Wooden Stick");
		Weapon woodenSword = new Weapon("Wooden Sword");
		Weapon javelin = new Weapon("Javelin");
		Weapon dagger = new Weapon("Dagger");
		Weapon ironSword = new Weapon("Iron Sword");
		Weapon steelSword = new Weapon("Steel Sword");
		Weapon katana = new Weapon("Katana");
		Weapon needleSword = new Weapon("Needle");
		Weapon dragonGlassSword = new Weapon("Dragon Glass Sword");
		Weapon poseidonTrident = new Weapon("Poseidon's Trident");
		Weapon oathKeeper = new Weapon("Oath Keeper");
		Weapon ice = new Weapon("Ice");
		Weapon excalibur = new Weapon("Excalibur");
		Weapon lightSaber = new Weapon("Lightsaber");
		Weapon elinathan = new Weapon("Elinathan");

		Armour rags = new Armour("Rags");
		Armour tShirt = new Armour("T-shirt (I <3 NY)");
		Armour leatherVest = new Armour("Leather Vest");
		Armour chainmail = new Armour("Chainmail");
		Armour steelPlate = new Armour("Steel Plate");
		Armour mithrilArmour = new Armour("Mithril Armour");
		Armour armourOfAchilles = new Armour("Armour of Achilles");
		Armour jediRobe = new Armour("Jedi Robe");
		Armour blessOfTheGods = new Armour("Bless of the Gods");
		
		
		Item badApple = new Item("Bad Apple");
		Item apple = new Item("Apple");
		Item badOrange = new Item("Bad Orange");
		Item orange = new Item("Orange");
		Item banana = new Item("Banana");
		Item milk = new Item("Milk");
		Item lamington = new Item("Lamington");
		Item pavlova = new Item("Pavlova");
		Item chocolate = new Item("Chocolate");
		Item vanillaSlice = new Item("Vanilla Slice");
		Item raspberry = new Item("Raspberry");
		*/
		//------------------------------------------------------
		//Adding enemies
		/*
		//Zone 1  ------------------------------------------
		Enemy babyGoblin = new Enemy("Baby Goblin");
		enemies.add(babyGoblin);
		babyGoblin.addDrop(badApple,0.50);
		babyGoblin.addDrop(badOrange,0.10);
		babyGoblin.addDrop(woodenStick,0.05);
		babyGoblin.addDrop(rags,0.05);
		
		Enemy babyWolf = new Enemy("Baby Wolf");
		enemies.add(babyWolf);
		babyWolf.addDrop(badApple,0.60);
		babyWolf.addDrop(badOrange,0.20);
		babyWolf.addDrop(woodenStick,0.075);
		babyWolf.addDrop(rags,0.075);
		
		Enemy babyBat = new Enemy("Baby Bat");
		enemies.add(babyBat);
		babyBat.addDrop(badApple,0.70);
		babyBat.addDrop(badOrange,0.30);
		babyBat.addDrop(woodenStick,0.08);
		babyBat.addDrop(woodenSword,0.01);
		babyBat.addDrop(rags,0.1);
		babyBat.addDrop(tShirt,0.02);
		
		Enemy babyRoo = new Enemy("Baby Roo");
		enemies.add(babyRoo);
		babyRoo.addDrop(badApple,0.80);
		babyRoo.addDrop(badOrange,0.40);
		babyRoo.addDrop(woodenStick,0.12);
		babyBat.addDrop(woodenSword,0.015);
		babyRoo.addDrop(rags,0.12);
		babyRoo.addDrop(tShirt,0.04);
		
		Enemy babyBear = new Enemy("Baby Bear");
		enemies.add(babyBear);
		babyBear.addDrop(badApple,0.90);
		babyBear.addDrop(badOrange,0.60);
		babyBear.addDrop(woodenStick,0.15);
		babyBear.addDrop(woodenSword,0.03);
		babyBear.addDrop(rags,0.14);
		babyBear.addDrop(tShirt,0.07);
		*/
		
		// Zone 2 -------------------------------------		
		Enemy goblin = new Enemy("Goblin");
		enemies.add(goblin);
		
		Enemy bat = new Enemy("Bat");
		enemies.add(bat);
		
		Enemy wildWolf = new Enemy("Wild Wolf");
		enemies.add(wildWolf);
		
		Enemy wildBear = new Enemy("Wild Bear");
		enemies.add(wildBear);
		
		Enemy wildRoo = new Enemy("Wild Roo");
		enemies.add(wildRoo);
		
		// Zone 3 -------------------------------------	
		Enemy scavenger = new Enemy("Scavenger");
		enemies.add(scavenger);
		
		Enemy babyGolem = new Enemy("Baby Golem");
		enemies.add(babyGolem);
		
		Enemy murloc = new Enemy("Murloc");
		enemies.add(murloc);
		
		Enemy babyOgre = new Enemy("Baby Ogre");
		enemies.add(babyOgre);
		
		Enemy creeper = new Enemy("Creeper");
		enemies.add(creeper);
		
		Enemy wildLion = new Enemy("Wild Lion");
		enemies.add(wildLion);
		
		// Zone 4 -------------------------------------	
		Enemy witch = new Enemy("Witch");
		enemies.add(witch);
		
		Enemy warlord = new Enemy("Warlord");
		enemies.add(warlord);
		
		Enemy tRex = new Enemy("T-Rex");
		enemies.add(tRex);
		
		Enemy dropBear = new Enemy("Drop Bear");
		enemies.add(dropBear);
		
		Enemy giantSpider = new Enemy("Giant Spider");
		enemies.add(giantSpider);
		
		Enemy velociraptor = new Enemy("Velociraptor");
		enemies.add(velociraptor);
		
		// Zone 5 -------------------------------------	
		Enemy chimera = new Enemy("Chimera");
		enemies.add(chimera);
		
		Enemy zombie = new Enemy("Zombie");
		enemies.add(zombie);
		
		Enemy heartless = new Enemy("Heartless");
		enemies.add(heartless);
		
		Enemy nobody = new Enemy("Nodoby");
		enemies.add(nobody);
		
		Enemy unversed = new Enemy("Unversed");
		enemies.add(unversed);
		
		Enemy dingo = new Enemy("Dingo");
		enemies.add(dingo);
		
		// Zone 6 -------------------------------------	
		Enemy golem = new Enemy("Golem");
		enemies.add(golem);
		
		Enemy ogre = new Enemy("Ogre");
		enemies.add(ogre);
		
		Enemy gohma = new Enemy("Gohma");
		enemies.add(gohma);
		
		Enemy poltergeist = new Enemy("Poltergeist");
		enemies.add(poltergeist);
		
		Enemy direWolf = new Enemy("Dire Wolf");
		enemies.add(direWolf);
		
		Enemy whiteWalker = new Enemy("White Walker");
		enemies.add(whiteWalker);
		
		// Zone 7 -------------------------------------	
		Enemy straga = new Enemy("Straga");
		enemies.add(straga);
		
		Enemy deathclaw = new Enemy("Deathclaw");
		enemies.add(deathclaw);
		
		Enemy hades = new Enemy("Hades");
		enemies.add(hades);
		
		Enemy reaper = new Enemy("Reaper");
		enemies.add(reaper);
		
		Enemy sith = new Enemy("Sith");
		enemies.add(sith);
		
		// Zone 8 -------------------------------------	
		Enemy weepingAngel = new Enemy("Weeping Angel");
		enemies.add(weepingAngel);
		
		Enemy dementor = new Enemy("Dementor");
		enemies.add(dementor);
		
		Enemy nemesis = new Enemy("Nemesis");
		enemies.add(nemesis);
		
		Enemy darthMaul = new Enemy("Darth Maul");
		enemies.add(darthMaul);
		
		Enemy hydra = new Enemy("Hydra");
		enemies.add(hydra);
		
		Enemy pyramidHead = new Enemy("Pyramid Head");
		enemies.add(pyramidHead);
		
		Enemy ganon = new Enemy("Ganon");
		enemies.add(ganon);
		
		// Dungeon 1  -------------------------------------	
		Enemy noirSnake = new Enemy("Noir Snake");
		enemies.add(noirSnake);
		
		Enemy gargoyle = new Enemy("Gargoyle");
		enemies.add(gargoyle);
		
		Enemy elite = new Enemy("Elite");
		enemies.add(elite);
		
		Enemy balverine = new Enemy("Balverine");
		enemies.add(balverine);
		
		Enemy crawler = new Enemy("Crawler"); // BOSS
		enemies.add(crawler);
		
		// Dungeon 2 -------------------------------------	
		Enemy cerberus = new Enemy("Cerberus");
		enemies.add(cerberus);
		
		Enemy leviathan = new Enemy("Leviathan");
		enemies.add(leviathan);
		
		Enemy ifrit = new Enemy("Ifrit");
		enemies.add(ifrit);
		
		Enemy shiva = new Enemy("Shiva");
		enemies.add(shiva);
		
		Enemy quetzacotl = new Enemy("Quetzacotl");
		enemies.add(quetzacotl);
		
		Enemy iznatron = new Enemy("Iznatron");
		enemies.add(iznatron);
		
		// Dungeon 3 -------------------------------------	
		Enemy devilsGuardian = new Enemy("Devil's Guardian");
		enemies.add(devilsGuardian);
		
		Enemy darkKnight = new Enemy("Dark Knight");
		enemies.add(darkKnight);
		
		Enemy viserion = new Enemy("Viserion");
		enemies.add(viserion);
		
		Enemy rhaegal = new Enemy("Rhaegal");
		enemies.add(rhaegal);
		
		Enemy drogon = new Enemy("Drogon");
		enemies.add(drogon);
		
		Enemy nightKing = new Enemy("Night King");
		enemies.add(nightKing);
		
		// Dungeon 4 -------------------------------------	
		Enemy blueEyesDragon = new Enemy("Blue Eyes Dragon");
		enemies.add(blueEyesDragon);
		
		Enemy daedra = new Enemy("Daedra");
		enemies.add(daedra);
		
		Enemy diablo = new Enemy("Diablo");
		enemies.add(diablo);
		
		Enemy sephirot = new Enemy("Sephirot");
		enemies.add(sephirot);
		
		Enemy darthVader = new Enemy("Darth Vader");
		enemies.add(darthVader);
		
		Enemy ultimaWeapon = new Enemy("Ultima Weapon");
		enemies.add(ultimaWeapon);
		
		Enemy darkCloud = new Enemy("Dark Cloud");
		enemies.add(darkCloud);
		
		Enemy darkSquall = new Enemy("Dark Squall");
		enemies.add(darkSquall);
		
		Enemy shadow = new Enemy("Shadow");
		enemies.add(shadow);
		
		//Adding temples
		/*
		Temple temple1 = new Temple("T1");
		Temple temple2 = new Temple("T2");
		Temple temple3 = new Temple("T3");
		temple1.setBoss(superLizard);
		temple1.addEnemies(guardianA);
		temple1.addEnemies(guardianB);
		
		temples.add(temple1);
		*/
		/*
		items.add(apple);
		items.add(orange);
		items.add(sPotion);
		hero.addItem(orange);
		ItemsHero tempItem = (ItemsHero) hero.findItem(orange);
		tempItem.increaseQuantity(1);
		hero.addItem(sPotion);
		tempItem = (ItemsHero) hero.findItem(sPotion);
		tempItem.increaseQuantity(1);
		*/
		//-------------------------------------------------

		
	}
	
	public void story()//Finds episode then sends you to main menu
	{
		episodeFinder();
		mainMenu();
	}
	
	//Main Menu -------------------------------------------------------------------------------------------------
	@SuppressWarnings("resource")
	public void mainMenu() 
	{
		Scanner key = new Scanner(System.in);
		boolean mainScreen = true;
		while(mainScreen)//Main screen for going to hero stats/save/explore
		{
			System.out.println("=====================================");
			System.out.println("1) " + hero.getName());
			System.out.println("2) Explore");
			System.out.println("3) Go to bed (Save)");
			
			String selection = key.nextLine();		
			if(selection.equalsIgnoreCase("1"))//Goes into Hero stats sub-menu
			{
				myStatsMenu();		
			}
			else if(selection.equalsIgnoreCase("2"))//Makes a random battle
			{
				explore();
			}		
			else if(selection.equalsIgnoreCase("3"))//Saves
			{
				save();
			}
			else {
				System.out.println("Enter a valid option");//Keeps main menu on loop
			}	
		}
	}
	
	//My Stats Menu (Option 1) ---------------------------------------------------------------------------------
	
	@SuppressWarnings("resource")
	public void myStatsMenu()
	{
		Scanner key = new Scanner(System.in);
		boolean myStatsMenu = true;
		while(myStatsMenu)
		{
			heroStats(hero);
			System.out.println();
			System.out.println("1) Upgrades");
			System.out.println("2) Equipment");
			System.out.println("3) Items");
			System.out.println("B) Back");
			
			String selection2 = key.nextLine();
				
			if(selection2.equalsIgnoreCase("1"))//Calls upgrades sub-menu
			{
				statsUpgradeMenu();
			}
			else if(selection2.equalsIgnoreCase("2"))//Calls equipments sub-menu
			{
				equipmentsMenu();
			}
			else if(selection2.equalsIgnoreCase("3"))//Calls items sub-menu
			{
				itemsMenu();
			}
			else if(selection2.equalsIgnoreCase("b"))//Exits hero submenu
			{
				myStatsMenu = false;
			}
		}		
	}
	
	@SuppressWarnings("resource")
	public void statsUpgradeMenu()//Menu option to use stat points
	{
		Scanner key = new Scanner(System.in);
		boolean shop = true;
		while(shop)//Start of menu
		{
			System.out.println("=====================================");
			System.out.println("Stat Points: " + hero.getStatPoints() + "\n");
			System.out.println("1) Increase Vitality");
			System.out.println("2) Increase Strength");
			System.out.println("3) Increase Agility");
			System.out.println("B) Back");
			String selection = key.nextLine();
			if(selection.equalsIgnoreCase("1"))//Vitality start
			{
				boolean correctInput = false;
				int points = 0;
				while(!correctInput)
				{				
					System.out.println("------------------");
					System.out.println("How many points you want to use? (MAX: " + hero.getStatPoints() + ")");
					try {
						points = key.nextInt();
						key.nextLine();
					}
					catch(Exception e)
					{
						System.out.println("Incorrect input");
						textToScreen("     ");
						return;
					}
					if(points<=hero.getStatPoints() && points>=0)
					{
						if(points == 0)
						{
							System.out.println("0... really?");
							textToScreen("     ");
							return;
						}
						else {
							correctInput = true;
						}		
					}
					else if(points<0)
					{
						System.out.println("Can't be negative numbers");
						textToScreen("     ");
						return;
					}
					else {
						System.out.println("You don't have enough stat points");
						textToScreen("     ");
						return;
					}
				}
				System.out.println("Are you sure (Vitality +" + points + ") ??(Y/N)");
				String buy = key.nextLine();
				if(buy.equalsIgnoreCase("y"))
				{
					System.out.println("Your Vitality has increased!");
					hero.increaseVit(points);
					hero.increaseStatPoints(-points);
					hero.calculateStats();
				}			
			}//End of Vitality 
			
			else if(selection.equalsIgnoreCase("2"))//Start of Strength
			{
				boolean correctInput = false;
				int points = 0;
				while(!correctInput)
				{				
					System.out.println("------------------");
					System.out.println("How many points you want to use? (MAX: " + hero.getStatPoints() + ")");
					try {
						points = key.nextInt();
						key.nextLine();
					}
					catch(Exception e)
					{
						System.out.println("Incorrect input");
						return;
					}
					if(points<=hero.getStatPoints() && points>=0)
					{
						correctInput = true;
					}
					else if(points<0)
					{
						System.out.println("Can't be negative numbers");
						return;
					}
					else {
						System.out.println("You don't have enough stat points");
						return;
					}
				}
				System.out.println("Are you sure (Strength +" + points + ") ??(Y/N)");
				String buy = key.nextLine();
				if(buy.equalsIgnoreCase("y"))
				{
					System.out.println("Your Strength has increased!");
					hero.increaseStr(points);
					hero.increaseStatPoints(-points);
					hero.calculateStats();
				}			
			}//End of Strength
			
			else if(selection.equalsIgnoreCase("3"))//Start of Agility
			{
				boolean correctInput = false;
				int points = 0;
				while(!correctInput)
				{				
					System.out.println("------------------");
					System.out.println("How many points you want to use? (MAX: " + hero.getStatPoints() + ")");
					try {
						points = key.nextInt();
						key.nextLine();
					}
					catch(Exception e)
					{
						System.out.println("Incorrect input");
						return;
					}
					if(points<=hero.getStatPoints() && points>=0)
					{
						correctInput = true;
					}
					else if(points<0)
					{
						System.out.println("Can't be negative numbers");
						return;
					}
					else {
						System.out.println("You don't have enough stat points");
						return;
					}
				}
				System.out.println("Are you sure (Agility +" + points + ") ??(Y/N)");
				String buy = key.nextLine();
				if(buy.equalsIgnoreCase("y"))
				{
					System.out.println("Your Agility has increased!");
					hero.increaseAgi(points);
					hero.increaseStatPoints(-points);
					hero.calculateStats();
				}			
			}//End of Agility
			
			else if(selection.equalsIgnoreCase("112233"))//For testing
			{
				hero.increaseVit(5);
				hero.increaseStr(5);
				hero.increaseAgi(5);
				System.out.println("All stats increased +5");
				hero.calculateStats();
			}
			else if(selection.equalsIgnoreCase("x"))//For testing
			{
				System.out.println("Exp increased");
				hero.increaseExperience(10000000);
			}
			else if(selection.equalsIgnoreCase("z"))//For testing
			{
				hero.increaseExperience(hero.getExperienceRemaining());
				hero.levelUp();
			}
			else if(selection.equalsIgnoreCase("b"))
			{
				return;
			}
		}
		
	}

	@SuppressWarnings("resource")
	public void equipmentsMenu()//Menu option to display owned Equipments
	{
		Scanner key = new Scanner(System.in);
		boolean equipmentShop = true;
		while(equipmentShop)
		{
			System.out.println("=========================");
			System.out.println("1) Armours");
			System.out.println("2) Swords");
			System.out.println("B) Back");
			String selection = key.nextLine();
			if(selection.equalsIgnoreCase("1"))//Start of Armour menu
			{
				boolean armourMenu = true;
				while(armourMenu)
				{
					int sizeArmour = hero.getArmoursOwned().size();
					if(sizeArmour == 0)//If empty does not access sub-menu
					{
						System.out.println("You have no armours");
						armourMenu = false;
					}
					else {
						List<Armour> armoursOwned = hero.getArmoursOwned(); 
						for(int x=0; x<sizeArmour; x++)
						{
							System.out.println(x+1 + ") " + armoursOwned.get(x).getName());//Prints names of all armours displaying index+1
						}
						System.out.println("--------------------------------");
						System.out.println("\nU) Unequip current armour");
						System.out.println("B) Back\n");
						String selection2 = key.nextLine();
						if(selection2.equalsIgnoreCase("b"))//Exits sub-menu
						{
							return;
						}
						else if(selection2.equalsIgnoreCase("u"))//Unequips current armour if not null equip 
						{
							if(hero.getArmourEquipped() == null)
							{
								System.out.println("You don't have anything equipped");
								textToScreen("     ");
								return;
							}
							else {
								hero.unequipArmour();
								textToScreen("     ");
								return;
							}
						}
						try {
							int index = Integer.parseInt(selection2);
							if((index>(sizeArmour)) || (index == 0))//Checks number is in the right range
							{
								System.out.println("Enter a valid number");
								textToScreen("     ");
								return;
							}
							else {
								Armour selectedArmour = armoursOwned.get(index-1);//Gets armour index-1
								boolean armourSubMenu = true;
								while(armourSubMenu)//Sub-menu for the selected armour
								{
									System.out.println("========================");
									System.out.println(selectedArmour.getName());
									System.out.println("========================");
									System.out.println("1) Equip");
									System.out.println("2) View Stats"); 
									System.out.println("B) Back"); 
									String armourOption = key.nextLine();
									if(armourOption.equalsIgnoreCase("b"))//Exits selected item
									{
										armourSubMenu = false;
									}
									else if(armourOption.equalsIgnoreCase("1"))//Equips item selected
									{
										if( (hero.getArmourEquipped() != null) && (hero.getArmourEquipped().equals(selectedArmour)) )//Checks if wearing same armour
										{
											System.out.println("That's the armour you are currently wearing");
										}
										else {
											hero.equipArmour(selectedArmour);
											System.out.println("You have equipped " + selectedArmour.getName());
										}				
									}
									else if(armourOption.equalsIgnoreCase("2"))//Prints item stats
									{
										System.out.println(selectedArmour);
									}
								}							
							}
						}
						catch(Exception e)
						{
							System.out.println("Enter a valid number");
						}
					}
					
				}
			}
			else if(selection.equalsIgnoreCase("2"))//Start of Weapons menu
			{
				boolean weaponMenu = true;
				while(weaponMenu)
				{
					int sizeWeapon = hero.getWeaponsOwned().size();//If empty returns to main menu
					if(sizeWeapon == 0)
					{
						System.out.println("You have no weapons");
						weaponMenu = false;
					}
					else {
						List<Weapon> weaponsOwned = hero.getWeaponsOwned(); 
						for(int x=0; x<sizeWeapon; x++)//Prints weapons owned
						{
							System.out.println(x+1 + ") " + weaponsOwned.get(x).getName());
						}
						System.out.println("--------------------------------");
						System.out.println("\nU) Unequip current weapon");
						System.out.println("B) Back\n");
						String selection2 = key.nextLine();
						if(selection2.equalsIgnoreCase("b"))//Exits sub-menu
						{
							return;
						}
						else if(selection2.equalsIgnoreCase("u"))//Unequips current weapon if not null
						{
							if(hero.getWeaponEquipped() == null)
							{
								System.out.println("You don't have anything equipped");
								textToScreen("     ");
								return;
							}
							else {
								hero.unequipWeapon();	
								textToScreen("     ");
								return;
							}
						}
						try {//Checking that input is a number
							int index = Integer.parseInt(selection2);
							if((index>(sizeWeapon)) || (index == 0))//Checks for correct range of number
							{
								System.out.println("Enter a valid number");
								textToScreen("     ");
								return;
							}
							else {
								Weapon selectedWeapon = weaponsOwned.get(index-1);//Selects weapon 
								boolean weaponSubMenu = true;
								while(weaponSubMenu)//Sub-Menu for selected Weapon
								{
									System.out.println("========================");
									System.out.println(selectedWeapon.getName());
									System.out.println("========================");
									System.out.println("1) Equip");
									System.out.println("2) View Stats"); 
									System.out.println("B) Back"); 
									String weaponOption = key.nextLine();
									if(weaponOption.equalsIgnoreCase("b"))//Exits selected weapon
									{
										weaponSubMenu = false;
									}
									else if(weaponOption.equalsIgnoreCase("1"))//Equips selected Weapon
									{
										if( (hero.getWeaponEquipped() != null) && (hero.getArmourEquipped().equals(selectedWeapon)) )//Checks if wearing same weapon
										{
											System.out.println("That's the armour you are currently wearing");
										}
										else {
											hero.equipWeapon(selectedWeapon);
											System.out.println("You have equipped " + selectedWeapon.getName());
										}	
									}
									else if(weaponOption.equalsIgnoreCase("2"))//Prints weapons details
									{
										System.out.println(selectedWeapon);
									}
								}							
							}
						}
						catch(Exception e)
						{
							System.out.println("Enter a valid number");//In case number out of bounds
						}
					}			
				}
			}
			else if(selection.equalsIgnoreCase("b"))//Exits Equipments Menu
			{
				return;
			}
		}
	}

	@SuppressWarnings("resource")
	public void itemsMenu()
	{
		boolean menu = true;
		Scanner key = new Scanner(System.in);
		while(menu)
		{
			boolean notEmpty = hero.showItems();//Display owned items/returns false if empty
			if(notEmpty)
			{
				System.out.println("B) Back");
				String selectionItem = key.nextLine();
				if(selectionItem.equalsIgnoreCase("b"))//Exits item menu
				{
					menu = false;
				}
				else {
					try {
						int indexTemp = Integer.parseInt(selectionItem);
						List<ItemsHero> itemsTemp = hero.getItems();
						if(indexTemp>itemsTemp.size() || indexTemp<1)//Verifies correct range of number
						{
							System.out.println("Incorrect number");
						}
						else {
							boolean itemSubMenu = true;
							while(itemSubMenu)//Once selected an item, starts its sub-menu
							{
								ItemsHero selectedItem = itemsTemp.get(indexTemp-1);
								System.out.println("=============================");
								System.out.println(selectedItem.getName() + "| Qty: " + selectedItem.getQuantity() );
								System.out.println("=============================\n");
								System.out.println("1) Use");
								System.out.println("2) Watch");
								System.out.println("B) Back");
								
								String subMenuSelection = key.nextLine();
								if(subMenuSelection.equalsIgnoreCase("1"))//Uses the item
								{
									System.out.println("You have used " + selectedItem.getName());
									selectedItem.increaseQuantity(-1);
									if(selectedItem.getType().equalsIgnoreCase("hp"))//If type HP, heals HP
									{
										double amountHealed;
										double heal = selectedItem.getPoints();
										if(heal>(1.1))
										{
											int healInt = (int)heal;
											amountHealed = hero.increaseCurrentHp(healInt);
										}
										else {
											amountHealed = hero.increaseCurrentHp(heal);
										}
										System.out.println("You have healed " + ((int)amountHealed));
									}
									if(selectedItem.getQuantity() == 0)//If reaches 0. delete items from list
									{
										itemsTemp.remove(indexTemp-1);
										itemSubMenu = false;
									}
								}
								else if(subMenuSelection.equalsIgnoreCase("2"))//Prints item attributes
								{
									System.out.println(selectedItem);
								}
								else if(subMenuSelection.equalsIgnoreCase("b"))//Exits sub-menu
								{
									itemSubMenu = false;
								}
							}	
						}
					}
					catch(Exception e) {}//Catches in case no number was typed
				}						
			}	
			else {
				menu = false;
			}
		}		
	}
	
	//Battle Loader --------------------------------------------------------------------------------
	public void battle(Hero tempHero)//Creates Battle
	{
		System.out.println("============= Battle Begins ======================");
		int minimum = 5;
		int maximum = 2;
		int randomNum = minimum + (int)(Math.random() * maximum); 
		Battle battle = new Battle(tempHero,enemies.get(randomNum));
		tempHero = battle.fight();
		updateHero(tempHero);
	}
	
	public void battle(Hero tempHero, Enemy enemy)//Creates Battle with specific enemy
	{
		System.out.println("============= Battle Begins ======================");
		Battle battle = new Battle(tempHero,enemy);
		tempHero = battle.fight();
		updateHero(tempHero);
	}
	
	//Option 2 Explore
	public void explore()
	{
		String mapActive = "";
		System.out.println("8)Up 2)Down 4)Left 6)Right");
		while(mapActive.equalsIgnoreCase(""))
		{
			map.displayMap();
			mapActive = map.movement();
			if(mapActive.equalsIgnoreCase(""))
			{
				double battleChance = Math.random();
				if(battleChance>0.50)
				{
					battle(hero);
					if(hero.getCurrentHp()<=0)
					{
						heroDies();
						return;
					}
				}
			}
		}	
		if(mapActive.equalsIgnoreCase("home"))
		{
			return;
		}
		else if(mapActive.equalsIgnoreCase("temple 1"))
		{
			temple(0);	
		}
		else if(mapActive.equalsIgnoreCase("temple 2"))
		{
			temple(1);
		}
		else if(mapActive.equalsIgnoreCase("temple 3"))
		{
			temple(2);
		}
	}
	
	//Utilities -------------------------------------------------------------------------------------------------
	@SuppressWarnings("resource")
	public void save()// Saves Session
	{
		Scanner key = new Scanner(System.in);
		System.out.println("Would you like to save? (Y/N)");
		String answer = key.nextLine();
		if(answer.equalsIgnoreCase("Y"))
		{
			while(true)
			{
				System.out.println("In which slot would you like to save? (1/2/3)");
				String slotN = key.nextLine();
				if((slotN.equalsIgnoreCase("1"))||(slotN.equalsIgnoreCase("2"))||(slotN.equalsIgnoreCase("3")))
				{
					try
					{
						String slot = "m" + slotN;
						FileOutputStream fos = new FileOutputStream(slot);
						ObjectOutputStream oos = new ObjectOutputStream(fos);
						oos.writeObject(this);
						oos.close();
						System.out.println("Game Succesfully Saved!");
						return;
					}
					catch(IOException e)
					{
						e.getStackTrace();
						System.out.println("Error 1 MyGame");
					}
					
				}
				System.out.println("Enter a valid number \n");
			}
		}
	}
	
	public void heroStats(Hero tempHero)// Prints Hero stats
	{
		System.out.println("=====================================");
		System.out.println(tempHero);
	}
		
	public void updateHero(Hero hero)//Used for refresh after battle
	{
		this.hero = hero;
	}
	
	public void heroDies()
	{
		int halfHp = hero.getMaxHp()/2;
		hero.setCurrentHp(halfHp);
		map.setCoords(22,12);
		map.refreshMap();
		
	}
	
	public static void textToScreen(String text)//Prints text slowly
	{
		int y = 0;
		for(int x=0;x<text.length();x++)
		{	
			if(text.charAt(x) == ' ' || text.charAt(x) == '.')
			{
				try {
					Thread.sleep(150);
				} catch (InterruptedException e) {
					System.out.println("Text produce Error");
				}			
				if(y>50)
				{
					System.out.println();
					y = 0;
				}
			}
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				System.out.println("Text produce Error");
			}			
			System.out.print(text.charAt(x));
			y++;
		}
		System.out.println();
		
	}
	
	//Episodes/Finders ------------------------------------------------------------------------------------------
	public void episodeFinder()//Loads episode 
	{
		if(episode == 1) {chapter1();}
		else if(episode == 2) {chapter2();}
		else {System.out.println("Error 2 MyGame");}		
	}
	
	public void chapter1()
	{
		String text = "Suddenly I wake up.... I am just by myself in this little house...I look through the window ... "
				+ "It's really dark outside..."
				+ "What should I do??";
		String text2 = "Testing ";
		textToScreen(text2);
		episode += 1;
	}
	
	public void chapter2()
	{
		String text = "A new day awaits me....";
		textToScreen(text);
	}
	
    public void temple(int index)
    {
    	Temple activeTemple = temples.get(index);
		if(activeTemple.getStatus().equalsIgnoreCase("cleared"))
		{
			
		}
		else {
			textToScreen("Entering temple " + (index+1) + "   ");
			for(int floor = 1; floor<=activeTemple.getFloors();floor++)
			{
				textToScreen("Floor " + floor + "  ");
				int indexEnemy = (int) (0.3*floor);
				battle(hero,activeTemple.getEnemy(indexEnemy));
				if(hero.getCurrentHp()<=0)
				{
					heroDies();
					return;
				}
			}
			textToScreen("Get ready for the boss!!");
			battle(hero,activeTemple.getBoss());
			if(hero.getCurrentHp()<=0)
			{
				heroDies();
				return;
			}
			textToScreen("You have finished temple " + (index+1) + "   ");
			activeTemple.cleared();
		}
    }
     
    public Enemy findEnemy(String name)
    {
    	for(Enemy x:enemies)
    	{
    		if(x.getName().equalsIgnoreCase(name))
    		{
    			return x;
    		}
    	}
    	return null;
    }
	
	//Getters ---------------------------------------------------------------------------------------------------
	public Hero getHero()
	{
		return hero;
	}
	
	public List<Enemy> getEnemies()
	{
		return enemies;
	}
	
	public List<Item> getItems()
	{
		return items;
	}
	
	public List<Temple> getTemples()
	{
		return temples;
	}
	
	//Object ----------------------------------------------------------------------------------------------------
	public String toString()
	{
		String s1 = "The hero of this story is " + hero.getName();
		return s1;
	}

}
