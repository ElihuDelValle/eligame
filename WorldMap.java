package eligame;

import java.util.Scanner;

public class WorldMap implements java.io.Serializable{

	private static final long serialVersionUID = 1L;
	
	private int coordsX;
	private int coordsY;
	private int sizeX;
	private int sizeY;
	private String[][] map;
	
	public WorldMap()
	{	
		coordsX = 12;
		coordsY = 22;
		sizeY = 40;
		sizeX = 90;
		createMap();
	}
	
	public void createMap()//Instantiate Map
	{
		map = new String[sizeY][sizeX];
		refreshMap();
	}
	
	public void move(int y,int x)//Moves hero
	{
		coordsX += x;
		coordsY += y;
		if(coordsX > (sizeX-2) || coordsX<1)
		{
			coordsX -= x;
			System.out.println("There's a wall dumbass");
		}
		if(coordsY > (sizeY-2) || coordsY<1)
		{
			coordsY -= y;
			System.out.println("There's a wall dumbass");
		}
	}
	
	public void refreshMap()//Refreshes Map
	{
		for(int x=0; x<map.length; x++)
		{
			for(int y=0; y<map[x].length;y++)
			{
				if( (x == 0) || (x == (map.length-1)) )
				{
					if( ((x==0) && (y==0)) || ((x==(map.length-1))&& (y==0)) || ((x==0)&&(y==(map[x].length-1)) ) || ((x==(map.length-1))&&(y==(map[x].length-1)) ))
					{
						map[x][y] = "X ";
					}
					else {
						map[x][y] = "X ";
						//map[x][y] = y + " ";
					}			
				}
				else {
					if( (y == 0) || (y == (map[x].length-1)) )
					{
						map[x][y] = "X ";
						//map[x][y] = x +" ";
					}
					else {
						map[x][y] = "  ";
					}
				}
			}
		}
		addBuildings();
		addMyself();
	}
	
	public void displayMap()//Prints map
	{
		for(int x=0; x<map.length; x++)
		{
			for(int y=0; y<map[x].length; y++)
			{
				System.out.print(map[x][y]);
			}
			System.out.println();
		}
	}
	
	public String afterMovement()//Checks if hero entered a building
	{
		refreshMap();
		if((coordsX) == 12 && (coordsY == 22))
		{
			return "home";
		}
		else if((coordsX == 27) && (coordsY == 12))
		{
			return "temple 3";
		}
		else if((coordsX == 18) && (coordsY == 6))
		{
			return "temple 2";
		}
		else if((coordsX == 2) && (coordsY == 2))
		{
			return "temple 1";
		}
		return "";
	}
	
	@SuppressWarnings("resource")
	public String movement()
	{
		Scanner key = new Scanner(System.in);
		String move = key.nextLine();
		if(move.equals("8"))
		{
			move(-1,0);
		}
		else if(move.equals("2"))
		{
			move(1,0);
		}
		else if(move.equals("4"))
		{
			move(0,-1);
		}
		else if(move.equals("6"))
		{
			move(0,1);
		}
		//----------------------------
		return afterMovement();
	}
	
	public void addBuildings()
	{
		addHouse();
		addTemples();
		addTerrain();
	}
	
	public void addHouse()
	{
		map[22][12] = "H ";
	}
	
	public void addTerrain()
	{
		for(int y = 0; y<sizeY; y++)
		{
			for(int x = 0; x<sizeX; x++)
			{
				if(map[y][x].equalsIgnoreCase("  "))
				{
					map[y][x] = "..";
				}
			}
		}
	}
	
	public void addTemples()
	{
		map[2][2] = "T1";
		map[12][27] = "T3";
		map[6][18] = "T2";
	}
	
	public void addMyself()
	{
		map[coordsY][coordsX] = "@ ";
	}

	public void setCoords(int y,int x)
	{
		coordsX = x;
		coordsY = y;
	}
	
}

